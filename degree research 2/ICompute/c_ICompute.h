#ifndef _ICOMP_
#define _ICOMP_

#include "../CoreUtilit/CoreUtilit.h"

enum eComputeType
{
	RK2_SC_GPU_VA_Lag_Diod,
	VIEWER,
};

struct s_paramColculate
{
	VECTOR3 directionE;
	VECTOR3 directionB;
	VECTOR3 directionStartV;
	float E;
	float B;
	float V;
	float y0;
	double T;
	unsigned int countSteps;
	float Density;
	float sizeCathodeX, sizeCathodeY;
	float sizeZ;
	unsigned int k;

	void operator=(s_paramColculate _t)
	{
		this->directionE = _t.directionE;
		this->directionB = _t.directionB;
		this->directionStartV = _t.directionStartV;
		this->E = _t.E;
		this->B = _t.B;
		this->V = _t.V;
		this->T = _t.T;
		this->countSteps = _t.countSteps;
		this->Density = _t.Density;
		this->sizeCathodeX = _t.sizeCathodeX; 
		this->sizeCathodeY = _t.sizeCathodeY;
		this->sizeZ = _t.sizeZ;
		this->k = _t.k;
		this->y0 = _t.y0;
	}
};

struct s_fullParamColculate
{
	VECTOR3 directionE;
	VECTOR3 directionB;
	VECTOR3 directionStartV;
	float E;
	float B;
	float V;
	double T;
	unsigned int countSteps;
	float Density;
	float sizeCathodeX, sizeCathodeY;
	float sizeZ;
	unsigned int k;
	DWORD computeTime;
	unsigned int countComputedSteps;


	void operator=(s_fullParamColculate _t)
	{
		this->directionE = _t.directionE;
		this->directionB = _t.directionB;
		this->directionStartV = _t.directionStartV;
		this->E = _t.E;
		this->B = _t.B;
		this->V = _t.V;
		this->T = _t.T;
		this->countSteps = _t.countSteps;
		this->Density = _t.Density;
		this->sizeCathodeX = _t.sizeCathodeX; 
		this->sizeCathodeY = _t.sizeCathodeY;
		this->k = _t.k;
		this->computeTime = _t.computeTime;
		this->countComputedSteps = _t.countComputedSteps;
		this->sizeZ = _t.sizeZ;
	}
	void operator=(s_paramColculate _t)
	{
		this->directionE = _t.directionE;
		this->directionB = _t.directionB;
		this->directionStartV = _t.directionStartV;
		this->E = _t.E;
		this->B = _t.B;
		this->V = _t.V;
		this->T = _t.T;
		this->countComputedSteps = _t.countSteps;
		this->Density = _t.Density;
		this->sizeCathodeX = _t.sizeCathodeX; 
		this->sizeCathodeY = _t.sizeCathodeY;
		this->k = _t.k;
		this->sizeZ = _t.sizeZ;
	}
};
__inline s_paramColculate FullParamToParam(s_fullParamColculate _t)
{
	s_paramColculate t;

	t.directionE = _t.directionE;
	t.directionB = _t.directionB;
	t.directionStartV = _t.directionStartV;
	t.E = _t.E;
	t.B = _t.B;
	t.V = _t.V;
	t.T = _t.T;
	t.countSteps = _t.countSteps + _t.countComputedSteps;
	t.Density = _t.Density;
	t.sizeCathodeX = _t.sizeCathodeX; 
	t.sizeCathodeY = _t.sizeCathodeY;
	t.k = _t.k;
	//t.y0 = _t.y0;

	return t;
}

__inline void F(VECTOR4* a, VECTOR4 currentV, VECTOR3 E, VECTOR3 B)
{
	float EV = (E.x*currentV.x + E.y*currentV.y + E.z*currentV.z)/8.9875517e+16f;
	float rel = -1.0f*sqrt(1.0f - (currentV.x*currentV.x + currentV.y*currentV.y + currentV.z*currentV.z)/8.9875517e+16f)*1.756e+11f;

	a->x = ( E.x + currentV.y*B.z - currentV.z*B.y - currentV.x*EV ) * rel;
	a->y = ( E.y + currentV.z*B.x - currentV.x*B.z - currentV.y*EV ) * rel;
	a->z = ( E.z + currentV.x*B.y - currentV.y*B.x - currentV.z*EV ) * rel;
}

static double e = -1.602e-19;		// ����� ���������
static double c = 2.99792458e+8;	// �������� �����
static double m = 9.11e-31;		// ����� ���������
static double m0 = 1.25663706e-6;	// ��������� ����������
static double e0 = 8.8541878e-12;	// ��������������� ������������� ������

__inline double TermLagField(double _t, double _tau, VECTOR4 _tCord, VECTOR4 _tauCord)
{
	float Rjx = _tCord.x - _tauCord.x;
	float Rjy = _tCord.y - _tauCord.y;
	float Rjz = _tCord.z - _tauCord.z;

	return _tau - _t + sqrt(Rjx*Rjx + Rjy*Rjy + Rjz*Rjz)/c;
}

class c_ICompute
{
public:
	
	vector<pair<int, DWORD>> m_computeTime;

	virtual void Create(s_paramColculate&) = 0;
	virtual void Create(s_fullParamColculate&) = 0;
	virtual void StartCompute() = 0;
	virtual void StopCompute() = 0;
	void Load(string _nameFile)
	{
		string _nameParFile = _nameFile;
		int tr = _nameParFile.find_last_of(".");
		_nameParFile.erase(tr,_nameParFile.size()-tr);
		_nameParFile = _nameParFile + ".par";

		ifstream inoutParFile(_nameParFile.c_str(), ios::in | ios::binary );

		s_fullParamColculate fullParamColculate;

		inoutParFile.read((char*)&fullParamColculate, sizeof(s_fullParamColculate));

		inoutParFile.close();

		m_paramColculate.B = fullParamColculate.B;
		m_paramColculate.countSteps = fullParamColculate.countSteps;
		m_paramColculate.Density = fullParamColculate.Density;
		m_paramColculate.directionB = fullParamColculate.directionB;
		m_paramColculate.directionE = fullParamColculate.directionE;
		m_paramColculate.directionStartV = fullParamColculate.directionStartV;
		m_paramColculate.E = fullParamColculate.E;
		m_paramColculate.k = fullParamColculate.k;
		m_paramColculate.sizeZ = fullParamColculate.sizeZ;
		m_paramColculate.sizeCathodeX = fullParamColculate.sizeCathodeX;
		m_paramColculate.sizeCathodeY = fullParamColculate.sizeCathodeY;
		m_paramColculate.T = fullParamColculate.T;
		m_paramColculate.V = fullParamColculate.V;

		ifstream inputFile(_nameFile.c_str(), ios::in | ios::binary);

		inputFile.read((char*)&m_paramColculate.countSteps, sizeof(unsigned int));

		for(unsigned int i = 0; i < m_paramColculate.countSteps; i++)
		{
			unsigned int tmp;
			inputFile.read((char*)&tmp, sizeof(unsigned int));
			m_Shift.insert(m_Shift.end(), tmp);

			char* dest = new char[255];
			sprintf(dest, " i = %d ", tmp );

			pLog->SetMessage(dest);
		}

		m_RangeX = 0.0f;
		m_RangeY = 0.0f;
		m_RangeZ = 0.0f;

		for(unsigned int i = 0; i < m_paramColculate.countSteps; i++)
		{
			double* tmp = new double[m_Shift[i]*3*3];
			inputFile.read((char*)tmp, sizeof(double)*m_Shift[i]*3*3);

			VECTOR4* _Velocity = new VECTOR4[m_Shift[i]];
			m_Velocity.push_back(_Velocity);

			VECTOR4* _Coordinate = new VECTOR4[m_Shift[i]];
			m_Coordinate.push_back(_Coordinate);

			VECTOR4* _Acceleration = new VECTOR4[m_Shift[i]];
			m_Acceleration.push_back(_Acceleration);

			for(int j = 0; j < m_Shift[i]; j++)
			{
				m_Coordinate[i][j].x = tmp[j*9];
				m_Coordinate[i][j].y = tmp[j*9+1];
				m_Coordinate[i][j].z = tmp[j*9+2];

				m_Velocity[i][j].x = tmp[j*9+3];
				m_Velocity[i][j].y = tmp[j*9+4];
				m_Velocity[i][j].z = tmp[j*9+5];

				m_Acceleration[i][j].x = tmp[j*9+6];
				m_Acceleration[i][j].y = tmp[j*9+7];
				m_Acceleration[i][j].z = tmp[j*9+8];
			}

			delete tmp;
		}

		for(unsigned int i = 0; i < m_paramColculate.countSteps; i++)
		{
			bool* tmp = new bool[m_Shift[i]];
			inputFile.read((char*)tmp, sizeof(bool)*m_Shift[i]);
			m_Activity.push_back(tmp);
			//delete tmp;
		}

		for(int j = 0; j < m_Shift[m_paramColculate.countSteps - 1]; j++)
		{
			if(m_Activity[m_paramColculate.countSteps - 1][j])
			{
				if(fabs(m_Coordinate[m_paramColculate.countSteps - 1][j].x) > fabs(m_RangeX))
					m_RangeX = fabs(m_Coordinate[m_paramColculate.countSteps - 1][j].x);
				if(fabs(m_Coordinate[m_paramColculate.countSteps - 1][j].y) > fabs(m_RangeY))
					m_RangeY = fabs(m_Coordinate[m_paramColculate.countSteps - 1][j].y);
				if(fabs(m_Coordinate[m_paramColculate.countSteps - 1][j].z) > fabs(m_RangeZ))
					m_RangeZ = fabs(m_Coordinate[m_paramColculate.countSteps - 1][j].z);
			}
		}
		inputFile.close();
	}

	bool Computed(){return m_Computed;}

	c_ICompute()
	{
		m_Computed = false;
		m_startCompTime = 0;
		m_curentCompTime = 0;
		m_startStep = 1;
	}
	~c_ICompute()
	{
		int s = m_Velocity.size();
		for(int i = 0; i < s; i++)
		{
			delete[] m_Velocity[i];
			delete[] m_Coordinate[i];
			delete[] m_Acceleration[i];
		}
		m_Velocity.clear();
		m_Coordinate.clear();
		m_Acceleration.clear();
	}

	float RangeX(){return m_RangeX;}
	float RangeY(){return m_RangeY;}
	float RangeZ(){return m_RangeZ;}


	DWORD computeTime()
	{
		return m_curentCompTime/CLOCKS_PER_SEC;
	}
	VECTOR3 directionE(){return m_paramColculate.directionE;}
	VECTOR3 directionB(){return m_paramColculate.directionB;}
	VECTOR3 directionStartV(){return m_paramColculate.directionStartV;}
	float E(){return m_paramColculate.E;}
	float B(){return m_paramColculate.B;}
	float V(){return m_paramColculate.V;}
	double T(){return m_paramColculate.T;}
	unsigned int countSteps(){return m_paramColculate.countSteps;}
	float Density(){return m_paramColculate.Density;}
	float sizeCathodeX(){return m_paramColculate.sizeCathodeX;}
	float sizeCathodeY(){if(m_paramColculate.sizeCathodeY > 0) return m_paramColculate.sizeCathodeY; else return m_paramColculate.sizeCathodeX; }
	float sizeZ(){return m_paramColculate.sizeZ;}
	unsigned int k(){return m_paramColculate.k;}
	float S()
	{
		if(m_paramColculate.sizeCathodeY > 0) 
			return m_paramColculate.sizeCathodeY*m_paramColculate.sizeCathodeX; 
		else 
			return m_paramColculate.sizeCathodeX*m_paramColculate.sizeCathodeX*3.1415f;
	}

	long maxCountParticle(){return m_maxCountParticle;}
	s_paramColculate* paramColculate(){return &m_paramColculate;}

	vector<VECTOR4*>* Coordinate(){return &m_Coordinate;}
	vector<VECTOR4*>* Velocity(){return &m_Velocity;}
	vector<VECTOR4*>* Acceleration(){return &m_Acceleration;}
	vector<unsigned int>* Shift(){return &m_Shift;}
	vector<bool*>* Activity(){return &m_Activity;}

	unsigned int countParticleAddedAtStap(){return m_countParticleAddedAtStap;}

	long curentCountParticls(){return m_curentCountParticls;}
	unsigned int curentCountSteps(){return m_curentCountSteps;}

protected:

	int lagResolve(VECTOR4 pos, long j, unsigned int t )
	{
		if( t > 0 )
		{
			double h = m_paramColculate.T;
			int minTk = j/m_countParticleAddedAtStap;
			int tau = t;
			double x;

			x = TermLagField(t*h, tau*h, pos, m_Coordinate[tau][j]);

			while( fabs(x) > h )
			{
				tau--;

				if(tau < minTk)
					return -1;
				else
					if(m_Activity[tau][j])
						x = TermLagField(t*h, tau*h, pos, m_Coordinate[tau][j]);
			}
			return tau;
		}
		return -1;
	}

	int lagResolve(long i , long j, unsigned int t )
	{
		if(i == j) return -1;
		return  lagResolve( m_Coordinate[t][i], j, t );
	}

	virtual void AddNewPart( int countPart, int cs)
	{
		VECTOR4* _V = new VECTOR4[m_curentCountParticls];
		m_Velocity.push_back(_V);

		VECTOR4* _C = new VECTOR4[m_curentCountParticls];
		m_Coordinate.push_back(_C);

		VECTOR4* _A = new VECTOR4[m_curentCountParticls];
		m_Acceleration.push_back(_A);

		srand(GetTickCount());
		float __t = - m_paramColculate.directionStartV.z*m_paramColculate.V*tan(m_paramColculate.y0*PI/180.0f)*2.0f/m_paramColculate.sizeCathodeY;
		for(int i = countPart; i < m_curentCountParticls; i++)
		{
			if(m_paramColculate.sizeCathodeY > 0)
			{
				m_Coordinate[cs][i] = VECTOR4(	(float)rand()*m_paramColculate.sizeCathodeX/RAND_MAX - m_paramColculate.sizeCathodeX/2.0f,
												(float)rand()*m_paramColculate.sizeCathodeY/RAND_MAX - m_paramColculate.sizeCathodeY/2.0f,
												(float)rand()*m_paramColculate.V*m_paramColculate.directionStartV.z*m_paramColculate.T/RAND_MAX,
												0.0f);
			}
			else
			{
				float z = (float)rand()*m_paramColculate.V*m_paramColculate.directionStartV.z*m_paramColculate.T/RAND_MAX;
				float r = m_paramColculate.sizeCathodeX;
				do
				{
					m_Coordinate[cs][i] = VECTOR4(	(float)rand()*r*2.0f/RAND_MAX - r,
													(float)rand()*r*2.0f/RAND_MAX - r,
													z,
													0.0f);
				} while(sqrt( m_Coordinate[cs][i].x*m_Coordinate[cs][i].x +  m_Coordinate[cs][i].y*m_Coordinate[cs][i].y) > r );
			}

			m_Velocity[cs][i] = VECTOR4(	0.0f,
											__t*m_Coordinate[cs][i].y,
											m_paramColculate.directionStartV.z*m_paramColculate.V,
											0.0f);
			
			m_Acceleration[cs][i] = VECTOR4(0.0, 0.0, 0.0, 0.0);
		}

		bool* _a = new bool[m_curentCountParticls];
		m_Activity.push_back(_a);
#pragma omp parallel for num_threads(3)
		for(int i = 0; i < m_curentCountParticls; i++)
			m_Activity[cs][i] = true;
	}

	float m_RangeX;
	float m_RangeY;
	float m_RangeZ;

	DWORD m_oldCompTime;
	DWORD m_curentCompTime;
	DWORD m_startCompTime;

	unsigned int m_startStep;

	bool m_Computed;

	s_paramColculate m_paramColculate;
	unsigned int m_countParticleAddedAtStap;
	long m_maxCountParticle;

	long m_curentCountParticls;
	unsigned int m_curentCountSteps;

	vector<VECTOR4*> m_Coordinate;
	vector<VECTOR4*> m_Velocity;
	vector<VECTOR4*> m_Acceleration;
	vector<unsigned int> m_Shift;
	vector<bool*> m_Activity;
};

__inline void Save(string _nameFile, c_ICompute* _iCompute)
{
	vector<unsigned int>* _shift = _iCompute->Shift();
	vector<VECTOR4*>* _coord = _iCompute->Coordinate();
	vector<VECTOR4*>* _v = _iCompute->Velocity();
	vector<VECTOR4*>* _a = _iCompute->Acceleration();
	vector<bool*>* _act = _iCompute->Activity();

	ofstream outputFile(_nameFile.c_str(), ios::out | ios::binary );

	unsigned int countSteps = _shift->size();
	outputFile.write((char*)&countSteps, sizeof(unsigned int));

	for(int i = 0; i < _shift->size(); i++)
	{
		outputFile.write((char*)&(*_shift)[i], sizeof(unsigned int));
	}

	for(unsigned int i = 0; i < _shift->size(); i++)
	{
		double* tmp = new double[(*_shift)[i]*3*3];
		for(int j = 0; j < (*_shift)[i]; j++)
		{
			tmp[j*9]   = (*_coord)[i][j].x;
			tmp[j*9+1] = (*_coord)[i][j].y;
			tmp[j*9+2] = (*_coord)[i][j].z;

			tmp[j*9+3] = (*_v)[i][j].x;
			tmp[j*9+4] = (*_v)[i][j].y;
			tmp[j*9+5] = (*_v)[i][j].z;

			tmp[j*9+6] = (*_a)[i][j].x;
			tmp[j*9+7] = (*_a)[i][j].y;
			tmp[j*9+8] = (*_a)[i][j].z;
		}
		outputFile.write((char*)tmp, sizeof(double)*(*_shift)[i]*3*3);
		outputFile.flush();
		delete tmp;
	}

	for(unsigned int i = 0; i < _shift->size(); i++)
	{
		outputFile.write((char*)(*_act)[i], sizeof(bool)*(*_shift)[i]);
		outputFile.flush();
	}

	outputFile.close();

	string _nameTxtFile = _nameFile;
	_nameTxtFile.erase(_nameTxtFile.size()-3,3);
	_nameTxtFile = _nameTxtFile + "txt";

	ofstream outputTxtFile(_nameTxtFile.c_str(), ios::out );

	outputTxtFile << "Bx = " << _iCompute->directionB().x << ";" << endl;
	outputTxtFile << "By = " << _iCompute->directionB().y << ";" << endl;
	outputTxtFile << "Bz = " << _iCompute->directionB().z << ";" << endl;
	outputTxtFile << "B = " << _iCompute->B() << "��;" << endl;
	outputTxtFile <<  endl;

	outputTxtFile << "Ex = " << _iCompute->directionE().x << ";" << endl;
	outputTxtFile << "Ey = " << _iCompute->directionE().y << ";" << endl;
	outputTxtFile << "Ez = " << _iCompute->directionE().z << ";" << endl;
	outputTxtFile << "E = " << _iCompute->E() << "�;" << endl;
	outputTxtFile <<  endl;

	outputTxtFile << "Vx = " << _iCompute->directionStartV().x << ";" << endl;
	outputTxtFile << "Vy = " << _iCompute->directionStartV().y << ";" << endl;
	outputTxtFile << "Vz = " << _iCompute->directionStartV().z << ";" << endl;
	outputTxtFile << "V = " << _iCompute->V() << "�/�;" << endl;
	outputTxtFile <<  endl;

	outputTxtFile << "��� �� ������� = " << _iCompute->T() << "�;" << endl;
	outputTxtFile << "���������� ����� = " << countSteps << ";" << endl;
	outputTxtFile << "������ �� ��� = " << _iCompute->countParticleAddedAtStap() << ";" << endl;
	outputTxtFile << "������ ������ �� X = " << _iCompute->sizeCathodeX() << "�;" << endl;
	outputTxtFile << "������ ������ �� Y = " << _iCompute->sizeCathodeY() << "�;" << endl;
	outputTxtFile << "������ ����� = " << _iCompute->sizeZ() << "�;" << endl;
	outputTxtFile << "����������� ���������� = " << _iCompute->k() << ";" << endl;
	outputTxtFile << "���������� ����������������� ������ �� ���� = " << _iCompute->Density() << ";" << endl;
	outputTxtFile << "����� ������� = ���� �� GPU � ������������� (������ 2) ;" << endl;

	outputTxtFile << "����� �������" << endl;
	for(int i = 0; i < _iCompute->m_computeTime.size(); i++)
		outputTxtFile << _iCompute->m_computeTime[i].first << " = " << _iCompute->m_computeTime[i].second/CLOCKS_PER_SEC << "�;" << endl;

	outputTxtFile.close();

	string _nameParFile = _nameFile;
	_nameParFile.erase(_nameParFile.size()-3,3);
	_nameParFile = _nameParFile + "par";

	ofstream outputParFile(_nameParFile.c_str(), ios::out | ios::binary );

	s_fullParamColculate fullParamColculate;

	fullParamColculate = *_iCompute->paramColculate();
	fullParamColculate.computeTime = _iCompute->computeTime();
	fullParamColculate.countSteps = 0;

	outputParFile.write((char*)&fullParamColculate, sizeof(s_fullParamColculate));

	outputParFile.close();

}

__inline float FindVKForVelocity(		VECTOR4* _Coordinate,
										VECTOR4* _Velocity,
										
										unsigned int _Shift,
										float length)
{
	int countPoint = 200;

	float* positiveCurrent = new float[countPoint];

	for(int i = 0; i < _Shift; i++)
	{
		int corPoint = _Coordinate[i].z/countPoint;
		if(corPoint < 0 || corPoint > countPoint)
			break;

		if(_Velocity[i].z > 0)
			positiveCurrent[corPoint] += e*_Velocity[i].z;
	}
	return 0;
}
__inline void computeCurrent(			VECTOR4* _Coordinate,
										VECTOR4* _Velocity,
										bool* _Activity,
										unsigned int _Shift,
										float* positiveCurrent,
										int countPoint,
										float k,
										float step,
										int direction = 1) // direction = 1 - all current, direction = 2 - forward current, direction = 3 - back current
{
	pLog->SetMessage("start Compute current");
	for(int i = 0; i < _Shift; i++)
	{
		int corPoint = _Coordinate[i].z/step;

		if(corPoint >= 0 && corPoint < countPoint && _Activity[i])
		{
			if(direction == 1)
				positiveCurrent[corPoint] += -k*e*_Velocity[i].z/step;	
			else if(direction == 2 && _Velocity[i].z > 0)
				positiveCurrent[corPoint] += -k*e*_Velocity[i].z/step;
			else if(direction == 3 && _Velocity[i].z < 0)
				positiveCurrent[corPoint] += -k*e*_Velocity[i].z/step;
		}
	}

#ifdef NDEBUG
	#pragma omp parallel for
#endif
	for(int c = 4; c < countPoint-4; c ++)
	{
		float sum = 0.0f;

		sum += positiveCurrent[c-4] * 0.05;
		sum += positiveCurrent[c-3] * 0.09;
		sum += positiveCurrent[c-2] * 0.12;
		sum += positiveCurrent[c-1] * 0.15;
		sum += positiveCurrent[c]  * 0.18;
		sum += positiveCurrent[c+1] * 0.15;
		sum += positiveCurrent[c+2] * 0.12;
		sum += positiveCurrent[c+3] * 0.09;
		sum += positiveCurrent[c+4] * 0.05;
		positiveCurrent[c] = sum;
	}

	pLog->SetMessage("end Compute current");
}
__inline float computeUWithoutLagField(	vector<VECTOR4*>& _Coordinate,
									   vector<bool*>& _Activity,
								int countPart,
								int t,
								float* _U,
								float k,
								float rX,
								float rY,
								float rZ,
								float step,
								int countSteps)
{
	float maxU = 0.0;

	int countstepX = rX/step + 1;
	int countstepY = rY/step + 1;
	int countstepZ = countSteps;

	float*** _f = new float** [countstepX];
	for (int i=0; i < countstepX; i++)
	{
		_f[i] = new float* [countstepY];
		for (int j=0; j < countstepY; j++)
			_f[i][j] = new float  [countstepZ];
	}

	float*** _U1 = new float** [countstepX];
	for (int i=0; i < countstepX; i++)
	{
		_U1[i] = new float* [countstepY];
		for (int j=0; j < countstepY; j++)
			_U1[i][j] = new float  [countstepZ];
	}

	float*** _U2 = new float** [countstepX];
	for (int i=0; i < countstepX; i++)
	{
		_U2[i] = new float* [countstepY];
		for (int j=0; j < countstepY; j++)
			_U2[i][j] = new float  [countstepZ];
	}

	for(int a = 0; a < countstepX; a ++)
	{
		for(int b = 0; b < countstepY; b ++)
		{
#pragma omp parallel for num_threads(3)
			for(int c = 0; c < countstepZ; c ++)
			{
				_f[a][b][c] = 0.0f;
				_U1[a][b][c] = 0.0f;
				_U2[a][b][c] = 0.0f;
				if((c==0)&&(fabs(b*step - rY/2.0) < 0.001/2.0)&&(fabs(a*step - rX/2.0) < 0.003/2.0))
					_f[a][b][c] = 0.05f/e0;
			}
		}
	}
	// ����������� ������� ����������� ����
	for(int r = 0; r < countPart; r++)
	{
		if(_Activity[t][r])
		{
			int _1 = (_Coordinate[t][r].x + rX/2.0)/step;
			int _2 = (_Coordinate[t][r].y + rY/2.0)/step;
			int _3 = _Coordinate[t][r].z/step;

			if( _1 >= 0 && _1 < countstepX &&
				_2 >= 0 && _2 < countstepY &&
				_3 >= 0 && _3 < countstepZ )
				_f[_1][_2][_3] += k*fabs(e)/(e0*pow(step,3.0f));//
		}
	}

	// ����������� ������� ����������� ����������
	/*for(int a = 0; a < countstepX; a ++)
	{
		for(int b = 0; b < countstepY; b ++)
		{
#pragma omp parallel for num_threads(3)
			for(int c = 0; c < countstepZ; c ++)
			{
				for(int l = 0; l < countPart; l++)
				{
					if(_Activity[t][l])
					{
						float r = (_Coordinate[t][l] - VECTOR4(a*step - rX/2.0, b*step - rY/2.0, c*step, 0.0)).length();
						_U1[a][b][c] += fabs(e)/(4.0*3.1415*e0*r*r);
					}
				}
				_U2[a][b][c] = _U1[a][b][c];
			}
		}
	}*/
/*
	// ���������� ���� ���� ���
	for(int a = 4; a < countstepX-4; a ++)
	{
		for(int b = 4; b < countstepY-4; b ++)
		{
#pragma omp parallel for num_threads(3)
			for(int c = 4; c < countstepZ-4; c ++)
			{
				float sum = 0.0f;
 
				// blur in y (vertical)
				// take nine samples, with the distance blurSize between them

				sum += _f[a-4][b][c] * 0.05;
				sum += _f[a-3][b][c] * 0.09;
				sum += _f[a-2][b][c] * 0.12;
				sum += _f[a-1][b][c] * 0.15;
				sum += _f[a][b][c]  * 0.18;
				sum += _f[a+1][b][c] * 0.15;
				sum += _f[a+2][b][c] * 0.12;
				sum += _f[a+3][b][c] * 0.09;
				sum += _f[a+4][b][c] * 0.05;
				_f[a][b][c] = sum;
			}
		}
	}
	for(int a = 4; a < countstepX-4; a ++)
	{
		for(int b = 4; b < countstepY-4; b ++)
		{
#pragma omp parallel for num_threads(3)
			for(int c = 4; c < countstepZ-4; c ++)
			{
				float sum = 0.0f;
 
				// blur in y (vertical)
				// take nine samples, with the distance blurSize between them

				sum += _f[a][b-4][c] * 0.05;
				sum += _f[a][b-3][c] * 0.09;
				sum += _f[a][b-2][c] * 0.12;
				sum += _f[a][b-1][c] * 0.15;
				sum += _f[a][b][c]  * 0.18;
				sum += _f[a][b+1][c] * 0.15;
				sum += _f[a][b+2][c] * 0.12;
				sum += _f[a][b+3][c] * 0.09;
				sum += _f[a][b+4][c] * 0.05;
				_f[a][b][c] = sum;
			}
		}
	}
	for(int a = 4; a < countstepX-4; a ++)
	{
#pragma omp parallel for num_threads(3)
		for(int b = 4; b < countstepY-4; b ++)
		{
			for(int c = 4; c < countstepZ-4; c ++)
			{
				float sum = 0.0f;
 
				// blur in y (vertical)
				// take nine samples, with the distance blurSize between them

				sum += _f[a][b][c-4] * 0.05;
				sum += _f[a][b][c-3] * 0.09;
				sum += _f[a][b][c-2] * 0.12;
				sum += _f[a][b][c-1] * 0.15;
				sum += _f[a][b][c]  * 0.18;
				sum += _f[a][b][c+1] * 0.15;
				sum += _f[a][b][c+2] * 0.12;
				sum += _f[a][b][c+3] * 0.09;
				sum += _f[a][b][c+4] * 0.05;
				_f[a][b][c] = sum;
			}
		}
	}*/

	// ������� ����
	int countIteration = 100;
	static bool param = true;
	
	
	for(int i = 0; i < countIteration; i++)
	{
		for(int a = 0; a < countstepX; a ++)
		{
			for(int b = 0; b < countstepY; b ++)
			{
#pragma omp parallel for num_threads(3)
				for(int c = 0; c < countstepZ; c ++)
				{
					float ***tmpU1, ***tmpU2;
					float tU1, tU2, tU3, tU4, tU5, tU6, D;

					if(param){
						tmpU1 = _U1;
						tmpU2 = _U2;
					}else{
						tmpU1 = _U2;
						tmpU2 = _U1;
					}

					tU1 = 0.0;
					tU2 = 0.0;
					tU3 = 0.0;
					tU4 = 0.0;
					tU5 = 0.0;
					tU6 = 0.0;
					D = 0.0;

					if(a>0)
						{tU1 = tmpU1[a-1][b][c]; D+=1.0;}
					if(a<countstepX-1)
						{tU2 = tmpU1[a+1][b][c]; D+=1.0;}
					if(b>0)
						{tU3 = tmpU1[a][b-1][c]; D+=1.0;}
					if(b<countstepY-1)
						{tU4 = tmpU1[a][b+1][c]; D+=1.0;}
					if(c>0)
						{tU5 = tmpU1[a][b][c-1]; D+=1.0;}
					if(c<countstepZ-1)
						{tU6 = tmpU1[a][b][c+1]; D+=1.0;}

					tmpU2[a][b][c] = (tU1 + tU2 + tU3 + tU4 + tU5 + tU6)/D + step*step*_f[a][b][c]/6;
				}
			}
		}
		param = !param;
	}

	if(param)
		memcpy( _U, _U1[countstepX/2][countstepY/2], countstepZ*sizeof(float));
	else
		memcpy( _U, _U2[countstepX/2][countstepY/2], countstepZ*sizeof(float));

	for(int y = 0; y < countstepZ; y++)
	{
		if(_U[y] > maxU)
			maxU = _U[y];
	}


	for (int i = 0; i < countstepX; i++)
	{
		for (int j=0; j < countstepY; j++)
			delete[] _f[i][j];
		delete[] _f[i];
	}
	delete[] _f;

	for (int i = 0; i < countstepX; i++)
	{
		for (int j=0; j < countstepY; j++)
			delete[] _U1[i][j];
		delete[] _U1[i];
	}
	delete[] _U1;

	for (int i = 0; i < countstepX; i++)
	{
		for (int j = 0; j < countstepY; j++)
			delete[] _U2[i][j];
		delete[] _U2[i];
	}
	delete[] _U2;

	return maxU;
}

__inline float computeUWithLagField(	vector<VECTOR4*>& _Coordinate,
										vector<VECTOR4*>& _Velocity,
										vector<VECTOR4*>& _Acceleration,
										int countPart,
										int t,
										float* _U,
										float k,
										double T,
										int countSteps,
										float step)
{
	float maxU = 0.0;


	VECTOR3* Ecs1 = new VECTOR3[countSteps];
	VECTOR3* Ecs2 = new VECTOR3[countSteps];
	VECTOR3* Ecs3 = new VECTOR3[countSteps];
	VECTOR3* Ecs4 = new VECTOR3[countSteps];
	VECTOR3* Ecs5 = new VECTOR3[countSteps];

	//			y
	//			^
	//			|
	//			1
	//		2	3	4	-->x
	//			5

	int _countParticleAddedAtStap = countPart/t;

#ifdef NDEBUG
	#pragma omp parallel for
#endif
	for(int i = 0; i < countSteps; i++)
	{		
		float Mul1 = 4.0f * PI * 8.8541878f / (-1.602e-7f * k);
		float Rjl, t1, t2;

		for(int l = 1; l < 6; l++)
		{
			VECTOR4 coord;
			switch(l)
			{
			case 1:
				coord = VECTOR4(0.0, step, i*step, 1.0);
				break;
			case 2:
				coord = VECTOR4(-step, 0.0, i*step, 1.0);
				break;
			case 3:
				coord = VECTOR4(0.0, 0.0, i*step, 1.0);
				break;
			case 4:
				coord = VECTOR4(step, 0.0, i*step, 1.0);
				break;
			case 5:
				coord = VECTOR4(0.0, -step, i*step, 1.0);
				break;
			};
			

			// ������� ������� ������������
			vector< pair< int, int > > _indexPart; // first - time, second - particle
			double h = T;
			for(int k = 0; k < countPart; k++)
			{
				if( i != k )
				{
					int tau = 0;//lagResolve( coord, k, t );
					
					if(tau >= 0)
						_indexPart.insert(_indexPart.end(),pair<int,int>(tau, k));
				}
			}

			// ���������� ���� ����������������� ������

			VECTOR3 t3, Ecsj, t4;

			for(int j = 0; j < _indexPart.size(); j++)
			{
				int J = _indexPart[j].second;
				int tau = _indexPart[j].first;

				VECTOR3 Rj;
				Rj.x = coord.x - _Coordinate[tau][J].x;
				Rj.y = coord.y - _Coordinate[tau][J].y;
				Rj.z = coord.z - _Coordinate[tau][J].z;

				Rjl = Rj.length();					

				if(Rjl > 0.00005)
				{
					t1 = Mul1*pow((Rjl - (Rj.x*_Velocity[tau][J].x + Rj.y*_Velocity[tau][J].y + Rj.z*_Velocity[tau][J].z)/c),3);
					t2 = 1.0f - (_Velocity[tau][J].x*_Velocity[tau][J].x + _Velocity[tau][J].y*_Velocity[tau][J].y + _Velocity[tau][J].z*_Velocity[tau][J].z)/(c*c);

					t4.x = Rj.x - Rjl*_Velocity[tau][J].x/c;
					t4.y = Rj.y - Rjl*_Velocity[tau][J].y/c;
					t4.z = Rj.z - Rjl*_Velocity[tau][J].z/c;

					t3 = (Rj % (t4 % VECTOR3(_Acceleration[tau][J].x, _Acceleration[tau][J].y, _Acceleration[tau][J].z)))/(c*c);

					Ecsj = (t4*t2 + t3)/t1;
					
					switch(l)
					{
					case 1:
						Ecs1[i] += Ecsj;
						break;
					case 2:
						Ecs2[i] += Ecsj;
						break;
					case 3:
						Ecs3[i] += Ecsj;
						break;
					case 4:
						Ecs4[i] += Ecsj;
						break;
					case 5:
						Ecs5[i] += Ecsj;
						break;
					};
				}
			}
		}
	}

	//			y
	//			^
	//			|
	//			1
	//		2	3	4	-->x
	//			5

	for(int i = 1; i < countSteps-1; i++)
	{
		_U[i] = step*(Ecs5[i].y + Ecs1[i].y + Ecs2[i].x + Ecs4[i].x + Ecs3[i-1].z + Ecs3[i+1].z);

		if(_U[i] > maxU)
			maxU = _U[i];
	}

	delete Ecs1;
	delete Ecs2;
	delete Ecs3;
	delete Ecs4;
	delete Ecs5;

	return maxU;
}

__inline float computeEnergy(	vector<VECTOR4*>& _Velocity,
								int countPart,
								int t)
{
	float energy = 0.0f;

	for(int i = 0; i < countPart; i++)
	{
		energy += m*_Velocity[t][i].x*_Velocity[t][i].x/(2*sqrt(1.0f - _Velocity[t][i].x*_Velocity[t][i].x/(c*c) )) + m*c*c;
		energy += m*_Velocity[t][i].y*_Velocity[t][i].y/(2*sqrt(1.0f - _Velocity[t][i].y*_Velocity[t][i].y/(c*c) )) + m*c*c;
		energy += m*_Velocity[t][i].z*_Velocity[t][i].z/(2*sqrt(1.0f - _Velocity[t][i].z*_Velocity[t][i].z/(c*c) )) + m*c*c;
	}
	return energy;
}
#endif