#ifndef _GPUINIT_
#define _GPUINIT_

#include "../CL/cl.h"
#include "../CoreUtilit/CoreUtilit.h"

__inline char* LoadProgSource(const char* cFilename, size_t* szFinalLength)
{
    // locals 
    FILE* pFileStream = NULL;
    size_t szSourceLength;

    if(fopen_s(&pFileStream, cFilename, "rb") != 0) 
	{ 
		string tmp = "failed open file ";
		tmp += cFilename;
		pLog->SetError( tmp , 3);
		return NULL; 
	}

    // get the length of the source code
    fseek(pFileStream, 0, SEEK_END); 
    szSourceLength = ftell(pFileStream);
    fseek(pFileStream, 0, SEEK_SET); 

    // allocate a buffer for the source code string and read it in
    char* cSourceString = (char *)malloc(szSourceLength +  1); 
    if (fread((cSourceString), szSourceLength, 1, pFileStream) != 1)
    {
        fclose(pFileStream);
        free(cSourceString);
        return 0;
    }
    // close the file and return the total length of the combined (preamble + source) string
    fclose(pFileStream);
    if(szFinalLength != 0){ *szFinalLength = szSourceLength; }
    cSourceString[szSourceLength] = '\0';

    return cSourceString;
}

class c_GPUInit
{
public:
	c_GPUInit(){};
	
protected:
	void InitGPU(const char* cSourceFile, int countKernels, char* nameKernels[])
	{
		int err = 0;

		cl_uint numPlatforms;
		cl_platform_id platform = NULL;
		err = clGetPlatformIDs(0, NULL, &numPlatforms);
		if(err!=CL_SUCCESS)
			return;
		if (0 < numPlatforms) 
		{
			cl_platform_id* platforms = new cl_platform_id[numPlatforms];
			err = clGetPlatformIDs(numPlatforms, platforms, NULL);
			if(err!=CL_SUCCESS)
				return;

			for (unsigned i = 0; i < numPlatforms; ++i) 
			{
				char pbuf[255];
				err = clGetPlatformInfo(platforms[i],
										   CL_PLATFORM_VENDOR,
										   sizeof(pbuf),
										   pbuf,
										   NULL);

				if(err!=CL_SUCCESS)
					return;
				pLog->SetMessage(pbuf);

				if (!strcmp(pbuf, "Advanced Micro Devices, Inc.")) 
					platform = platforms[i]; // TODO ������� ������ ����� ��������� !!!

				err = clGetPlatformInfo(platforms[i],
										   CL_PLATFORM_VERSION,
										   sizeof(pbuf),
										   pbuf,
										   NULL);

				if(err!=CL_SUCCESS)
					return;
				pLog->SetMessage(pbuf);

				err = clGetPlatformInfo(platforms[i],
										   CL_PLATFORM_NAME,
										   sizeof(pbuf),
											   pbuf,
										   NULL);

				if(err!=CL_SUCCESS)
					return;
				pLog->SetMessage(pbuf);

				

			}
			delete[] platforms;
		}


		if(NULL == platform)
		{
			pLog->SetError("NULL platform found so Exiting Application.", 3);
			return;
		}


		cl_context_properties cps[3] = 
		{
			CL_CONTEXT_PLATFORM, 
			(cl_context_properties)platform, 
			0
		};

		size_t ParmDataBytes;
		GPUContext = clCreateContextFromType(cps, CL_DEVICE_TYPE_ALL, NULL, NULL, &err);
		
		err += clGetContextInfo(GPUContext, CL_CONTEXT_DEVICES, 0, NULL, &ParmDataBytes);
		GPUDevices = (cl_device_id*)malloc(ParmDataBytes);
		err += clGetContextInfo(GPUContext, CL_CONTEXT_DEVICES, ParmDataBytes, GPUDevices, NULL);

		char device_string[1024];
		err += clGetDeviceInfo(GPUDevices[0], CL_DEVICE_NAME, sizeof(device_string), &device_string, NULL);

		err += clGetDeviceInfo(GPUDevices[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &m_maxWorkGroupSize, NULL);

		int imageSupport = 0;
		err += clGetDeviceInfo(GPUDevices[0], CL_DEVICE_IMAGE_SUPPORT, sizeof(size_t), &imageSupport, NULL);
		if(imageSupport == 0)
		{
			pLog->SetError("device not support images", 2);
			//exit(1);
		}

		
		GPUCommandQueue = clCreateCommandQueue(GPUContext, GPUDevices[0], 0, &err);
		if(err!=CL_SUCCESS)
			pLog->SetError("clCreateCommandQueue error", 3);


		//***********************INIT KERNEL***********************

		size_t szKernelLength;

		char* cSourceCL = LoadProgSource(cSourceFile, &szKernelLength);
		cl_program clProgram = clCreateProgramWithSource(GPUContext, 1, (const char **)&cSourceCL, &szKernelLength, &err);
		
		err = clBuildProgram(clProgram, 0, NULL, NULL, NULL, NULL);
		//Print error-message
		if(err!=CL_SUCCESS){
			char programLog[4096];
			err = clGetProgramBuildInfo(clProgram, GPUDevices[0],CL_PROGRAM_BUILD_LOG, 4096, programLog, 0);
			pLog->SetMessage(programLog);
			
			exit(1);
		}
		
		clKernel = new cl_kernel[countKernels];
		for(int i = 0; i<countKernels; i++)
			clKernel[i] = clCreateKernel(clProgram, nameKernels[i], NULL);

	}

	cl_context GPUContext;
	cl_device_id* GPUDevices;
	cl_command_queue GPUCommandQueue;

	size_t m_maxWorkGroupSize;
	cl_kernel* clKernel;
};

#endif