inline float4 mul(float4 A, float4 B)
{
	float4 tmp;
	tmp.x = A.y*B.z - A.z*B.y;
	tmp.y = A.z*B.x - A.x*B.z;
	tmp.z = A.x*B.y - A.y*B.x;
	tmp.w = 0.0f;
	return tmp;
}

inline float4 FforV( float4 currentV, float4 E, float4 B)
{
	float4 a;
	float EV = (E.x*currentV.x + E.y*currentV.y + E.z*currentV.z)/8.9875517e+16f;
	float rel = -1.758e+11f*sqrt(1.0f - (currentV.x*currentV.x + currentV.y*currentV.y + currentV.z*currentV.z)/8.9875517e+16f);

	a.x = ( E.x + currentV.y*B.z - currentV.z*B.y - currentV.x*EV ) * rel;
	a.y = ( E.y + currentV.z*B.x - currentV.x*B.z - currentV.y*EV ) * rel;
	a.z = ( E.z + currentV.x*B.y - currentV.y*B.x - currentV.z*EV ) * rel;
	return a;
}

__kernel void KernelSC(	
							__read_only image2d_t A,
							__read_only image2d_t V,
							__read_only image2d_t C,
							__global int* countPartJ,
							__global float4* E,
							__global float4* B,
							const int offset,
							const float mult
						)
{
	sampler_t  samp = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

	int idx = get_global_id(0);

	const float c = 2.99792458e+8f;
	
	float4 Rj, tmp1, _V, _A, _C, _tC, tmp3;
	float Src, Rjl, tmp, tmp2;
	float4 tE = {0.0f, 0.0f, 0.0f, 0.0f}, tB = {0.0f, 0.0f, 0.0f, 0.0f};
	int2 coord = {0, idx};
	
	int countPart = countPartJ[idx];
	while(coord.x < countPart)
	{	
		_V = read_imagef(V, samp, coord ).xyzw;
		_C = read_imagef(C, samp, coord ).xyzw;
		_A = read_imagef(A, samp, coord ).xyzw;
		
		coord.x++;
		
		_tC.x = _V.w;
		_tC.y = _C.w;
		_tC.z = _A.w;

		_V.w = 0.0f;
		_C.w = 0.0f;
		_A.w = 0.0f;
		_tC.w = 0.0f;

		Rj = _tC - _C; Rj.w = 0.0f;
		Rjl = length(Rj);
		
		if(Rjl > 0.0005000)
		{
			Src = (Rjl - dot(Rj, _V)/c); 
			tmp = mult * Src * Src * Src;
			tmp2 = 1.0f - dot(_V, _V)/(c*c);
			tmp3 = (Rj - _V * Rjl / c);
			tmp1 = mul(Rj, mul(tmp3, _A));
			
			float4 ttE = (tmp3 * tmp2 + tmp1/(c*c))/ tmp;
			tE += ttE;
			tB += mul(Rj, ttE)/(c*Rjl);
		}
	}
	E[idx + offset] += tE;		
	B[idx + offset] += tB; 
}

// ����� ����� 2
__kernel void KernelRK2(	__global float4* FC,
							__global float4* FV,
							__global float4* E,
							__global float4* B,
							__global float4* SV,
							__global float4* SC,
							__global float4* SA,
							const float t
							)
{
	int index = get_global_id(0);
	
	float4 f0, f1, tmpV;
	float4 tmp1, tmp2, tmp3;
	
	tmp1 = FV[index];
	tmp2 = E[index]; tmp2.w = 0.0f;
	tmp3 = B[index]; tmp3.w = 0.0f;
					
	f0 = FforV( tmp1, tmp2, tmp3);
	f1 = FforV( tmp1 + f0*t, tmp2, tmp3);
	SA[index] = f0;

	tmpV = tmp1 + (f0 + f1)*t/2.0f;
	SC[index] = FC[index] + tmpV*t;
	SV[index] = tmpV;
}


// �����
/*__kernel void KernelRK2(	__global float4* FC,
							__global float4* FV,
							__global float4* E,
							__global float4* B,
							__global float4* SV,
							__global float4* SC,
							__global float4* SA,
							const float t
							)
{
	int index = get_global_id(0);
	
	float4 f0, f1, f2, f3, tmpV;
	float4 tmp1, tmp2, tmp3;
	
	tmp1 = FV[index];
	tmp2 = E[index]; tmp2.w = 0.0f;
	tmp3 = B[index]; tmp3.w = 0.0f;
					
	f0 = FforV( tmp1,			tmp2, tmp3);

	SA[index] = f0;

	tmpV = tmp1 + f0*t;
	SC[index] = FC[index] + tmpV*t;
	SV[index] = tmpV;
}*/

// ����� ����� 4
/*__kernel void KernelRK2(	__global float4* FC,
							__global float4* FV,
							__global float4* E,
							__global float4* B,
							__global float4* SV,
							__global float4* SC,
							__global float4* SA,
							const float t
							)
{
	int index = get_global_id(0);
	
	float4 f0, f1, f2, f3, tmpV;
	float4 tmp1, tmp2, tmp3;
	
	tmp1 = FV[index];
	tmp2 = E[index]; tmp2.w = 0.0f;
	tmp3 = B[index]; tmp3.w = 0.0f;
					
	f0 = FforV( tmp1,			tmp2, tmp3);
	f1 = FforV( tmp1 + f0*0.5f*t, tmp2, tmp3);
	f2 = FforV( tmp1 + f1*0.5f*t, tmp2, tmp3);
	f3 = FforV( tmp1 + f2*t,		tmp2, tmp3);

	SA[index] = f0;

	tmpV = tmp1 + (f0 + 2.0f*f1 + 2.0f*f2 + f3)*t/6.0f;
	SC[index] = FC[index] + tmpV*t;
	SV[index] = tmpV;
}*/

__kernel void KernelEBInit(		__global float4* E,
								__global float4* B,
								const float4 _E,
								const float4 _B
							)
{
	int index = get_global_id(0);
	
	E[index] = _E;	
	B[index] = _B; 
}

