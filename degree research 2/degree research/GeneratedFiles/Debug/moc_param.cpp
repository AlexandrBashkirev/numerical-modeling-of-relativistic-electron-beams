/****************************************************************************
** Meta object code from reading C++ file 'param.h'
**
** Created: Tue 13. Aug 17:06:36 2013
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../param.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'param.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_param[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x08,
      23,    6,    6,    6, 0x08,
      42,    6,    6,    6, 0x08,
      68,    6,    6,    6, 0x08,
      99,    6,    6,    6, 0x08,
     130,    6,    6,    6, 0x08,
     161,    6,    6,    6, 0x08,
     190,    6,    6,    6, 0x08,
     210,    6,    6,    6, 0x08,
     231,    6,    6,    6, 0x08,
     251,    6,    6,    6, 0x08,
     274,  272,    6,    6, 0x08,
     298,  272,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_param[] = {
    "param\0\0on_ok_clicked()\0on_close_clicked()\0"
    "on_Density_valueChanged()\0"
    "on_sizeCathodeX_valueChanged()\0"
    "on_sizeCathodeY_valueChanged()\0"
    "on_sizeCathodeR_valueChanged()\0"
    "on_countSteps_valueChanged()\0"
    "on_k_valueChanged()\0on_Vz_valueChanged()\0"
    "on_V_valueChanged()\0on_y0_valueChanged()\0"
    "t\0on_squere_toggled(bool)\0"
    "on_circle_toggled(bool)\0"
};

const QMetaObject param::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_param,
      qt_meta_data_param, 0 }
};

const QMetaObject *param::metaObject() const
{
    return &staticMetaObject;
}

void *param::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_param))
        return static_cast<void*>(const_cast< param*>(this));
    return QDialog::qt_metacast(_clname);
}

int param::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_ok_clicked(); break;
        case 1: on_close_clicked(); break;
        case 2: on_Density_valueChanged(); break;
        case 3: on_sizeCathodeX_valueChanged(); break;
        case 4: on_sizeCathodeY_valueChanged(); break;
        case 5: on_sizeCathodeR_valueChanged(); break;
        case 6: on_countSteps_valueChanged(); break;
        case 7: on_k_valueChanged(); break;
        case 8: on_Vz_valueChanged(); break;
        case 9: on_V_valueChanged(); break;
        case 10: on_y0_valueChanged(); break;
        case 11: on_squere_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: on_circle_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        }
        _id -= 13;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
