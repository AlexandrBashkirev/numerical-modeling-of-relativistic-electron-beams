/****************************************************************************
** Meta object code from reading C++ file 'degreeresearch.h'
**
** Created: Tue 13. Aug 17:06:36 2013
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../degreeresearch.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'degreeresearch.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_degreeresearch[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      39,   15,   15,   15, 0x08,
      59,   15,   15,   15, 0x08,
      79,   15,   15,   15, 0x08,
      97,   15,   15,   15, 0x08,
     117,   15,   15,   15, 0x08,
     137,   15,   15,   15, 0x08,
     158,   15,   15,   15, 0x08,
     187,   15,   15,   15, 0x08,
     211,  209,   15,   15, 0x08,
     255,  249,   15,   15, 0x08,
     280,   15,   15,   15, 0x08,
     298,   15,   15,   15, 0x08,
     317,   15,   15,   15, 0x08,
     338,   15,   15,   15, 0x08,
     359,   15,   15,   15, 0x08,
     380,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_degreeresearch[] = {
    "degreeresearch\0\0on_NewComp_triggered()\0"
    "on_Open_triggered()\0on_Save_triggered()\0"
    "on_Up_triggered()\0on_Down_triggered()\0"
    "on_Left_triggered()\0on_Right_triggered()\0"
    "on_FinishCompute_triggered()\0"
    "on_FindVK_triggered()\0i\0"
    "on_horizontalSlider_valueChanged(int)\0"
    "event\0closeEvent(QCloseEvent*)\0"
    "on_stop_clicked()\0on_pause_clicked()\0"
    "on_ZOX_toggled(bool)\0on_ZOY_toggled(bool)\0"
    "on_XOY_toggled(bool)\0updateRenderInfo()\0"
};

const QMetaObject degreeresearch::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_degreeresearch,
      qt_meta_data_degreeresearch, 0 }
};

const QMetaObject *degreeresearch::metaObject() const
{
    return &staticMetaObject;
}

void *degreeresearch::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_degreeresearch))
        return static_cast<void*>(const_cast< degreeresearch*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int degreeresearch::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_NewComp_triggered(); break;
        case 1: on_Open_triggered(); break;
        case 2: on_Save_triggered(); break;
        case 3: on_Up_triggered(); break;
        case 4: on_Down_triggered(); break;
        case 5: on_Left_triggered(); break;
        case 6: on_Right_triggered(); break;
        case 7: on_FinishCompute_triggered(); break;
        case 8: on_FindVK_triggered(); break;
        case 9: on_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: closeEvent((*reinterpret_cast< QCloseEvent*(*)>(_a[1]))); break;
        case 11: on_stop_clicked(); break;
        case 12: on_pause_clicked(); break;
        case 13: on_ZOX_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: on_ZOY_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: on_XOY_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: updateRenderInfo(); break;
        }
        _id -= 17;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
