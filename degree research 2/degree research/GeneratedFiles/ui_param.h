/********************************************************************************
** Form generated from reading ui file 'param.ui'
**
** Created: Tue 13. Aug 17:06:36 2013
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_PARAM_H
#define UI_PARAM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGroupBox>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_paramClass
{
public:
    QPushButton *ok;
    QPushButton *close;
    QLabel *label;
    QLabel *label_2;
    QDoubleSpinBox *Ex;
    QDoubleSpinBox *Ey;
    QDoubleSpinBox *Ez;
    QDoubleSpinBox *Bx;
    QDoubleSpinBox *By;
    QDoubleSpinBox *Bz;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QDoubleSpinBox *E;
    QDoubleSpinBox *B;
    QLabel *label_6;
    QLabel *label_7;
    QDoubleSpinBox *valueT;
    QLabel *label_8;
    QSpinBox *powerT;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QDoubleSpinBox *Vx;
    QDoubleSpinBox *Vy;
    QDoubleSpinBox *Vz;
    QDoubleSpinBox *V;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_26;
    QLabel *label_28;
    QDoubleSpinBox *sizeCathodeX;
    QDoubleSpinBox *sizeCathodeY;
    QSpinBox *countSteps;
    QLabel *label_18;
    QSpinBox *k;
    QLabel *label_22;
    QDoubleSpinBox *Density;
    QGroupBox *groupBox_2;
    QLabel *label_19;
    QLabel *perveance;
    QLabel *label_21;
    QLabel *I0;
    QLabel *label_29;
    QLabel *particlePerStep;
    QLabel *label_31;
    QLabel *maxParticle;
    QLabel *label_20;
    QLabel *streamLength;
    QLabel *Zkr;
    QLabel *label_30;
    QLabel *label_27;
    QDoubleSpinBox *y0;
    QLabel *label_32;
    QDoubleSpinBox *sizeZ;
    QGroupBox *groupBox;
    QRadioButton *squere;
    QRadioButton *circle;
    QLabel *label_33;
    QLabel *label_34;
    QDoubleSpinBox *sizeCathodeR;
    QLabel *label_35;

    void setupUi(QWidget *paramClass)
    {
    if (paramClass->objectName().isEmpty())
        paramClass->setObjectName(QString::fromUtf8("paramClass"));
    paramClass->resize(433, 788);
    ok = new QPushButton(paramClass);
    ok->setObjectName(QString::fromUtf8("ok"));
    ok->setGeometry(QRect(250, 740, 75, 23));
    ok->setContextMenuPolicy(Qt::NoContextMenu);
    close = new QPushButton(paramClass);
    close->setObjectName(QString::fromUtf8("close"));
    close->setGeometry(QRect(340, 740, 75, 23));
    label = new QLabel(paramClass);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(30, 50, 21, 16));
    label_2 = new QLabel(paramClass);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setGeometry(QRect(30, 80, 16, 21));
    Ex = new QDoubleSpinBox(paramClass);
    Ex->setObjectName(QString::fromUtf8("Ex"));
    Ex->setGeometry(QRect(70, 50, 62, 22));
    Ex->setDecimals(4);
    Ex->setMinimum(-99);
    Ex->setSingleStep(0.1);
    Ey = new QDoubleSpinBox(paramClass);
    Ey->setObjectName(QString::fromUtf8("Ey"));
    Ey->setGeometry(QRect(150, 50, 62, 22));
    Ey->setDecimals(4);
    Ey->setMinimum(-99);
    Ey->setSingleStep(0.1);
    Ey->setValue(-1);
    Ez = new QDoubleSpinBox(paramClass);
    Ez->setObjectName(QString::fromUtf8("Ez"));
    Ez->setGeometry(QRect(230, 50, 62, 22));
    Ez->setDecimals(4);
    Ez->setMinimum(-99);
    Ez->setSingleStep(0.1);
    Bx = new QDoubleSpinBox(paramClass);
    Bx->setObjectName(QString::fromUtf8("Bx"));
    Bx->setGeometry(QRect(70, 80, 62, 22));
    Bx->setDecimals(4);
    Bx->setMinimum(-99);
    Bx->setSingleStep(0.1);
    Bx->setValue(1);
    By = new QDoubleSpinBox(paramClass);
    By->setObjectName(QString::fromUtf8("By"));
    By->setGeometry(QRect(150, 80, 62, 22));
    By->setDecimals(4);
    By->setMinimum(-99);
    By->setSingleStep(0.1);
    Bz = new QDoubleSpinBox(paramClass);
    Bz->setObjectName(QString::fromUtf8("Bz"));
    Bz->setGeometry(QRect(230, 80, 62, 22));
    Bz->setDecimals(4);
    Bz->setMinimum(-99);
    Bz->setSingleStep(0.1);
    label_3 = new QLabel(paramClass);
    label_3->setObjectName(QString::fromUtf8("label_3"));
    label_3->setGeometry(QRect(90, 20, 16, 16));
    label_4 = new QLabel(paramClass);
    label_4->setObjectName(QString::fromUtf8("label_4"));
    label_4->setGeometry(QRect(170, 20, 21, 16));
    label_5 = new QLabel(paramClass);
    label_5->setObjectName(QString::fromUtf8("label_5"));
    label_5->setGeometry(QRect(250, 20, 16, 16));
    E = new QDoubleSpinBox(paramClass);
    E->setObjectName(QString::fromUtf8("E"));
    E->setGeometry(QRect(320, 50, 62, 22));
    E->setContextMenuPolicy(Qt::PreventContextMenu);
    E->setMaximum(1e+09);
    E->setValue(1.2e+08);
    B = new QDoubleSpinBox(paramClass);
    B->setObjectName(QString::fromUtf8("B"));
    B->setGeometry(QRect(320, 80, 62, 22));
    B->setDecimals(6);
    B->setMaximum(1.5);
    B->setSingleStep(0.1);
    B->setValue(0.5);
    label_6 = new QLabel(paramClass);
    label_6->setObjectName(QString::fromUtf8("label_6"));
    label_6->setGeometry(QRect(40, 410, 111, 21));
    label_7 = new QLabel(paramClass);
    label_7->setObjectName(QString::fromUtf8("label_7"));
    label_7->setGeometry(QRect(40, 460, 91, 16));
    valueT = new QDoubleSpinBox(paramClass);
    valueT->setObjectName(QString::fromUtf8("valueT"));
    valueT->setGeometry(QRect(180, 460, 51, 22));
    valueT->setDecimals(2);
    valueT->setMinimum(1);
    valueT->setMaximum(9.99);
    valueT->setValue(1);
    label_8 = new QLabel(paramClass);
    label_8->setObjectName(QString::fromUtf8("label_8"));
    label_8->setGeometry(QRect(240, 460, 46, 21));
    powerT = new QSpinBox(paramClass);
    powerT->setObjectName(QString::fromUtf8("powerT"));
    powerT->setGeometry(QRect(260, 440, 42, 22));
    powerT->setMinimum(1);
    powerT->setValue(12);
    label_9 = new QLabel(paramClass);
    label_9->setObjectName(QString::fromUtf8("label_9"));
    label_9->setGeometry(QRect(250, 440, 16, 16));
    label_10 = new QLabel(paramClass);
    label_10->setObjectName(QString::fromUtf8("label_10"));
    label_10->setGeometry(QRect(390, 40, 16, 16));
    label_11 = new QLabel(paramClass);
    label_11->setObjectName(QString::fromUtf8("label_11"));
    label_11->setGeometry(QRect(390, 50, 16, 16));
    label_12 = new QLabel(paramClass);
    label_12->setObjectName(QString::fromUtf8("label_12"));
    label_12->setGeometry(QRect(390, 60, 16, 16));
    label_13 = new QLabel(paramClass);
    label_13->setObjectName(QString::fromUtf8("label_13"));
    label_13->setGeometry(QRect(390, 80, 21, 16));
    label_14 = new QLabel(paramClass);
    label_14->setObjectName(QString::fromUtf8("label_14"));
    label_14->setGeometry(QRect(30, 110, 16, 21));
    Vx = new QDoubleSpinBox(paramClass);
    Vx->setObjectName(QString::fromUtf8("Vx"));
    Vx->setGeometry(QRect(70, 110, 62, 22));
    Vx->setDecimals(4);
    Vx->setMinimum(-99);
    Vx->setSingleStep(0.1);
    Vy = new QDoubleSpinBox(paramClass);
    Vy->setObjectName(QString::fromUtf8("Vy"));
    Vy->setGeometry(QRect(150, 110, 62, 22));
    Vy->setDecimals(4);
    Vy->setMinimum(-99);
    Vy->setSingleStep(0.1);
    Vz = new QDoubleSpinBox(paramClass);
    Vz->setObjectName(QString::fromUtf8("Vz"));
    Vz->setGeometry(QRect(230, 110, 62, 22));
    Vz->setDecimals(4);
    Vz->setMinimum(-99);
    Vz->setSingleStep(0.1);
    Vz->setValue(1);
    V = new QDoubleSpinBox(paramClass);
    V->setObjectName(QString::fromUtf8("V"));
    V->setGeometry(QRect(320, 110, 62, 22));
    V->setMaximum(1e+09);
    V->setValue(2.4e+08);
    label_15 = new QLabel(paramClass);
    label_15->setObjectName(QString::fromUtf8("label_15"));
    label_15->setGeometry(QRect(390, 100, 16, 16));
    label_16 = new QLabel(paramClass);
    label_16->setObjectName(QString::fromUtf8("label_16"));
    label_16->setGeometry(QRect(390, 110, 16, 16));
    label_17 = new QLabel(paramClass);
    label_17->setObjectName(QString::fromUtf8("label_17"));
    label_17->setGeometry(QRect(390, 120, 16, 16));
    label_23 = new QLabel(paramClass);
    label_23->setObjectName(QString::fromUtf8("label_23"));
    label_23->setGeometry(QRect(170, 240, 41, 31));
    label_24 = new QLabel(paramClass);
    label_24->setObjectName(QString::fromUtf8("label_24"));
    label_24->setGeometry(QRect(290, 240, 16, 31));
    label_25 = new QLabel(paramClass);
    label_25->setObjectName(QString::fromUtf8("label_25"));
    label_25->setGeometry(QRect(390, 240, 21, 31));
    label_26 = new QLabel(paramClass);
    label_26->setObjectName(QString::fromUtf8("label_26"));
    label_26->setGeometry(QRect(210, 240, 20, 31));
    label_28 = new QLabel(paramClass);
    label_28->setObjectName(QString::fromUtf8("label_28"));
    label_28->setGeometry(QRect(310, 240, 16, 31));
    sizeCathodeX = new QDoubleSpinBox(paramClass);
    sizeCathodeX->setObjectName(QString::fromUtf8("sizeCathodeX"));
    sizeCathodeX->setGeometry(QRect(220, 240, 61, 22));
    sizeCathodeX->setDecimals(4);
    sizeCathodeX->setMinimum(0.0001);
    sizeCathodeX->setMaximum(1);
    sizeCathodeX->setSingleStep(0.001);
    sizeCathodeX->setValue(0.003);
    sizeCathodeY = new QDoubleSpinBox(paramClass);
    sizeCathodeY->setObjectName(QString::fromUtf8("sizeCathodeY"));
    sizeCathodeY->setGeometry(QRect(320, 240, 61, 22));
    sizeCathodeY->setDecimals(4);
    sizeCathodeY->setMinimum(0.0001);
    sizeCathodeY->setMaximum(1);
    sizeCathodeY->setSingleStep(0.001);
    sizeCathodeY->setValue(0.001);
    countSteps = new QSpinBox(paramClass);
    countSteps->setObjectName(QString::fromUtf8("countSteps"));
    countSteps->setGeometry(QRect(160, 410, 71, 22));
    countSteps->setMinimum(10);
    countSteps->setMaximum(10000000);
    countSteps->setSingleStep(10);
    countSteps->setValue(100);
    label_18 = new QLabel(paramClass);
    label_18->setObjectName(QString::fromUtf8("label_18"));
    label_18->setGeometry(QRect(40, 500, 101, 16));
    k = new QSpinBox(paramClass);
    k->setObjectName(QString::fromUtf8("k"));
    k->setGeometry(QRect(180, 500, 81, 22));
    k->setMinimum(1);
    k->setMaximum(2000000000);
    k->setSingleStep(1000000);
    k->setValue(7000000);
    label_22 = new QLabel(paramClass);
    label_22->setObjectName(QString::fromUtf8("label_22"));
    label_22->setGeometry(QRect(30, 160, 211, 21));
    Density = new QDoubleSpinBox(paramClass);
    Density->setObjectName(QString::fromUtf8("Density"));
    Density->setGeometry(QRect(250, 160, 71, 22));
    Density->setDecimals(5);
    Density->setMinimum(0);
    Density->setSingleStep(0.01);
    Density->setValue(0.05);
    groupBox_2 = new QGroupBox(paramClass);
    groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
    groupBox_2->setGeometry(QRect(30, 560, 381, 171));
    label_19 = new QLabel(groupBox_2);
    label_19->setObjectName(QString::fromUtf8("label_19"));
    label_19->setGeometry(QRect(20, 20, 51, 16));
    perveance = new QLabel(groupBox_2);
    perveance->setObjectName(QString::fromUtf8("perveance"));
    perveance->setGeometry(QRect(110, 20, 161, 16));
    label_21 = new QLabel(groupBox_2);
    label_21->setObjectName(QString::fromUtf8("label_21"));
    label_21->setGeometry(QRect(20, 40, 46, 14));
    I0 = new QLabel(groupBox_2);
    I0->setObjectName(QString::fromUtf8("I0"));
    I0->setGeometry(QRect(110, 40, 46, 14));
    label_29 = new QLabel(groupBox_2);
    label_29->setObjectName(QString::fromUtf8("label_29"));
    label_29->setGeometry(QRect(20, 60, 81, 16));
    particlePerStep = new QLabel(groupBox_2);
    particlePerStep->setObjectName(QString::fromUtf8("particlePerStep"));
    particlePerStep->setGeometry(QRect(130, 60, 46, 14));
    label_31 = new QLabel(groupBox_2);
    label_31->setObjectName(QString::fromUtf8("label_31"));
    label_31->setGeometry(QRect(20, 80, 101, 16));
    maxParticle = new QLabel(groupBox_2);
    maxParticle->setObjectName(QString::fromUtf8("maxParticle"));
    maxParticle->setGeometry(QRect(130, 80, 46, 14));
    label_20 = new QLabel(groupBox_2);
    label_20->setObjectName(QString::fromUtf8("label_20"));
    label_20->setGeometry(QRect(20, 100, 81, 16));
    streamLength = new QLabel(groupBox_2);
    streamLength->setObjectName(QString::fromUtf8("streamLength"));
    streamLength->setGeometry(QRect(130, 100, 46, 14));
    Zkr = new QLabel(groupBox_2);
    Zkr->setObjectName(QString::fromUtf8("Zkr"));
    Zkr->setGeometry(QRect(130, 130, 151, 16));
    label_30 = new QLabel(groupBox_2);
    label_30->setObjectName(QString::fromUtf8("label_30"));
    label_30->setGeometry(QRect(20, 130, 81, 16));
    label_27 = new QLabel(paramClass);
    label_27->setObjectName(QString::fromUtf8("label_27"));
    label_27->setGeometry(QRect(40, 530, 101, 16));
    y0 = new QDoubleSpinBox(paramClass);
    y0->setObjectName(QString::fromUtf8("y0"));
    y0->setGeometry(QRect(180, 530, 51, 22));
    y0->setDecimals(0);
    y0->setMinimum(-30);
    y0->setMaximum(30);
    y0->setValue(0);
    label_32 = new QLabel(paramClass);
    label_32->setObjectName(QString::fromUtf8("label_32"));
    label_32->setGeometry(QRect(40, 370, 61, 31));
    sizeZ = new QDoubleSpinBox(paramClass);
    sizeZ->setObjectName(QString::fromUtf8("sizeZ"));
    sizeZ->setGeometry(QRect(110, 370, 61, 22));
    sizeZ->setDecimals(4);
    sizeZ->setMinimum(0.001);
    sizeZ->setMaximum(1);
    sizeZ->setSingleStep(0.01);
    sizeZ->setValue(0.1);
    groupBox = new QGroupBox(paramClass);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    groupBox->setGeometry(QRect(10, 230, 131, 80));
    squere = new QRadioButton(groupBox);
    squere->setObjectName(QString::fromUtf8("squere"));
    squere->setGeometry(QRect(20, 20, 101, 18));
    squere->setChecked(true);
    circle = new QRadioButton(groupBox);
    circle->setObjectName(QString::fromUtf8("circle"));
    circle->setGeometry(QRect(20, 50, 84, 18));
    label_33 = new QLabel(paramClass);
    label_33->setObjectName(QString::fromUtf8("label_33"));
    label_33->setGeometry(QRect(170, 280, 41, 31));
    label_34 = new QLabel(paramClass);
    label_34->setObjectName(QString::fromUtf8("label_34"));
    label_34->setGeometry(QRect(210, 280, 20, 31));
    sizeCathodeR = new QDoubleSpinBox(paramClass);
    sizeCathodeR->setObjectName(QString::fromUtf8("sizeCathodeR"));
    sizeCathodeR->setGeometry(QRect(220, 280, 61, 22));
    sizeCathodeR->setDecimals(4);
    sizeCathodeR->setMinimum(0.0001);
    sizeCathodeR->setMaximum(1);
    sizeCathodeR->setSingleStep(0.001);
    sizeCathodeR->setValue(0.003);
    label_35 = new QLabel(paramClass);
    label_35->setObjectName(QString::fromUtf8("label_35"));
    label_35->setGeometry(QRect(290, 280, 16, 31));

    retranslateUi(paramClass);

    QMetaObject::connectSlotsByName(paramClass);
    } // setupUi

    void retranslateUi(QWidget *paramClass)
    {
    paramClass->setWindowTitle(QApplication::translate("paramClass", "param", 0, QApplication::UnicodeUTF8));
    ok->setText(QApplication::translate("paramClass", "\320\276\320\272", 0, QApplication::UnicodeUTF8));
    close->setText(QApplication::translate("paramClass", "close", 0, QApplication::UnicodeUTF8));
    label->setText(QApplication::translate("paramClass", "E", 0, QApplication::UnicodeUTF8));
    label_2->setText(QApplication::translate("paramClass", "B", 0, QApplication::UnicodeUTF8));
    label_3->setText(QApplication::translate("paramClass", "x", 0, QApplication::UnicodeUTF8));
    label_4->setText(QApplication::translate("paramClass", "y", 0, QApplication::UnicodeUTF8));
    label_5->setText(QApplication::translate("paramClass", "z", 0, QApplication::UnicodeUTF8));
    label_6->setText(QApplication::translate("paramClass", "\320\232\320\276\320\273\320\270\321\207\320\265\321\201\321\202\320\262\320\276 \321\210\320\260\320\263\320\276\320\262", 0, QApplication::UnicodeUTF8));
    label_7->setText(QApplication::translate("paramClass", "\320\250\320\260\320\263 \320\277\320\276 \320\262\321\200\320\265\320\274\320\265\320\275\320\270", 0, QApplication::UnicodeUTF8));
    label_8->setText(QApplication::translate("paramClass", "* 10", 0, QApplication::UnicodeUTF8));
    label_9->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_10->setText(QApplication::translate("paramClass", "\320\222", 0, QApplication::UnicodeUTF8));
    label_11->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_12->setText(QApplication::translate("paramClass", "\320\274", 0, QApplication::UnicodeUTF8));
    label_13->setText(QApplication::translate("paramClass", "\320\242\320\273", 0, QApplication::UnicodeUTF8));
    label_14->setText(QApplication::translate("paramClass", "V", 0, QApplication::UnicodeUTF8));
    label_15->setText(QApplication::translate("paramClass", "\320\274", 0, QApplication::UnicodeUTF8));
    label_16->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_17->setText(QApplication::translate("paramClass", "\321\201", 0, QApplication::UnicodeUTF8));
    label_23->setText(QApplication::translate("paramClass", "\320\232\320\260\321\202\320\276\320\264:", 0, QApplication::UnicodeUTF8));
    label_24->setText(QApplication::translate("paramClass", "\320\274", 0, QApplication::UnicodeUTF8));
    label_25->setText(QApplication::translate("paramClass", "\320\274", 0, QApplication::UnicodeUTF8));
    label_26->setText(QApplication::translate("paramClass", "X:", 0, QApplication::UnicodeUTF8));
    label_28->setText(QApplication::translate("paramClass", "Y:", 0, QApplication::UnicodeUTF8));
    label_18->setText(QApplication::translate("paramClass", "\320\232\320\276\321\215\321\204. \321\203\320\272\321\200\321\203\320\277\320\275\320\265\320\275\320\270\321\217:", 0, QApplication::UnicodeUTF8));
    label_22->setText(QApplication::translate("paramClass", "\320\237\320\273\320\276\321\202\320\275\320\276\321\201\321\202\321\214 \320\277\321\200\320\276\321\201\321\202\321\200\320\260\320\275\321\201\321\202\320\262\320\265\320\275\320\275\320\276\320\263\320\276 \320\267\320\260\321\200\321\217\320\264\320\260 :", 0, QApplication::UnicodeUTF8));
    groupBox_2->setTitle(QApplication::translate("paramClass", "\320\277\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213", 0, QApplication::UnicodeUTF8));
    label_19->setText(QApplication::translate("paramClass", "\320\237\320\265\321\200\320\262\320\265\320\260\320\275\321\201:", 0, QApplication::UnicodeUTF8));
    perveance->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_21->setText(QApplication::translate("paramClass", "\320\242\320\276\320\272:", 0, QApplication::UnicodeUTF8));
    I0->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_29->setText(QApplication::translate("paramClass", "\320\247\320\260\321\201\321\202\320\270\321\206 \320\267\320\260 \321\210\320\260\320\263:", 0, QApplication::UnicodeUTF8));
    particlePerStep->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_31->setText(QApplication::translate("paramClass", "\320\234\320\260\320\272\321\201\320\270\320\274\321\203\320\274 \321\207\320\260\321\201\321\202\320\270\321\206:", 0, QApplication::UnicodeUTF8));
    maxParticle->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_20->setText(QApplication::translate("paramClass", "\320\224\320\273\320\270\320\275\320\275\320\260 \320\277\320\276\321\202\320\276\320\272\320\260:", 0, QApplication::UnicodeUTF8));
    streamLength->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    Zkr->setText(QApplication::translate("paramClass", "-", 0, QApplication::UnicodeUTF8));
    label_30->setText(QApplication::translate("paramClass", "Z\320\272\321\200", 0, QApplication::UnicodeUTF8));
    label_27->setText(QApplication::translate("paramClass", "\320\243\320\263\320\276\320\273 \320\262\320\273\321\221\321\202\320\260", 0, QApplication::UnicodeUTF8));
    label_32->setText(QApplication::translate("paramClass", "\320\236\320\261\320\273. \320\262\320\267\320\260\320\270\320\274:", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("paramClass", "\320\244\320\276\321\200\320\274\320\260 \320\272\320\260\321\202\320\276\320\264\320\260", 0, QApplication::UnicodeUTF8));
    squere->setText(QApplication::translate("paramClass", "\320\237\321\200\321\217\320\274\320\276\321\203\320\263\320\276\320\273\321\214\320\275\320\270\320\272", 0, QApplication::UnicodeUTF8));
    circle->setText(QApplication::translate("paramClass", "\320\232\321\200\321\203\320\263", 0, QApplication::UnicodeUTF8));
    label_33->setText(QApplication::translate("paramClass", "\320\232\320\260\321\202\320\276\320\264:", 0, QApplication::UnicodeUTF8));
    label_34->setText(QApplication::translate("paramClass", "R:", 0, QApplication::UnicodeUTF8));
    label_35->setText(QApplication::translate("paramClass", "\320\274", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(paramClass);
    } // retranslateUi

};

namespace Ui {
    class paramClass: public Ui_paramClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PARAM_H
