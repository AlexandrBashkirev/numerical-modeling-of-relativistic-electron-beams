/********************************************************************************
** Form generated from reading ui file 'vkcomputemethod.ui'
**
** Created: Tue 13. Aug 17:06:36 2013
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_VKCOMPUTEMETHOD_H
#define UI_VKCOMPUTEMETHOD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VKComputeMethodClass
{
public:

    void setupUi(QWidget *VKComputeMethodClass)
    {
    if (VKComputeMethodClass->objectName().isEmpty())
        VKComputeMethodClass->setObjectName(QString::fromUtf8("VKComputeMethodClass"));
    VKComputeMethodClass->resize(400, 300);

    retranslateUi(VKComputeMethodClass);

    QMetaObject::connectSlotsByName(VKComputeMethodClass);
    } // setupUi

    void retranslateUi(QWidget *VKComputeMethodClass)
    {
    VKComputeMethodClass->setWindowTitle(QApplication::translate("VKComputeMethodClass", "VKComputeMethod", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(VKComputeMethodClass);
    } // retranslateUi

};

namespace Ui {
    class VKComputeMethodClass: public Ui_VKComputeMethodClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VKCOMPUTEMETHOD_H
