/********************************************************************************
** Form generated from reading ui file 'countstepsfinishcomp.ui'
**
** Created: Tue 13. Aug 17:06:36 2013
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_COUNTSTEPSFINISHCOMP_H
#define UI_COUNTSTEPSFINISHCOMP_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_countStepsFinishCompClass
{
public:
    QPushButton *close;
    QPushButton *ok;
    QSpinBox *countSteps;
    QLabel *label;

    void setupUi(QWidget *countStepsFinishCompClass)
    {
    if (countStepsFinishCompClass->objectName().isEmpty())
        countStepsFinishCompClass->setObjectName(QString::fromUtf8("countStepsFinishCompClass"));
    countStepsFinishCompClass->resize(244, 132);
    close = new QPushButton(countStepsFinishCompClass);
    close->setObjectName(QString::fromUtf8("close"));
    close->setGeometry(QRect(130, 80, 75, 23));
    ok = new QPushButton(countStepsFinishCompClass);
    ok->setObjectName(QString::fromUtf8("ok"));
    ok->setGeometry(QRect(40, 80, 75, 23));
    countSteps = new QSpinBox(countStepsFinishCompClass);
    countSteps->setObjectName(QString::fromUtf8("countSteps"));
    countSteps->setGeometry(QRect(90, 50, 71, 22));
    countSteps->setMinimum(1);
    countSteps->setMaximum(10000);
    countSteps->setSingleStep(100);
    countSteps->setValue(100);
    label = new QLabel(countStepsFinishCompClass);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(50, 20, 141, 16));

    retranslateUi(countStepsFinishCompClass);

    QMetaObject::connectSlotsByName(countStepsFinishCompClass);
    } // setupUi

    void retranslateUi(QWidget *countStepsFinishCompClass)
    {
    countStepsFinishCompClass->setWindowTitle(QApplication::translate("countStepsFinishCompClass", "countStepsFinishComp", 0, QApplication::UnicodeUTF8));
    close->setText(QApplication::translate("countStepsFinishCompClass", "close", 0, QApplication::UnicodeUTF8));
    ok->setText(QApplication::translate("countStepsFinishCompClass", "ok", 0, QApplication::UnicodeUTF8));
    label->setText(QApplication::translate("countStepsFinishCompClass", "\320\241\320\272\320\276\320\273\321\214\320\272\320\276 \321\210\320\260\320\263\320\276\320\262 \320\264\320\276\321\201\321\207\320\270\321\202\320\260\321\202\321\214?", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(countStepsFinishCompClass);
    } // retranslateUi

};

namespace Ui {
    class countStepsFinishCompClass: public Ui_countStepsFinishCompClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COUNTSTEPSFINISHCOMP_H
