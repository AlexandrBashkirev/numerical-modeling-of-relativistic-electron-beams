/********************************************************************************
** Form generated from reading ui file 'degreeresearch.ui'
**
** Created: Tue 13. Aug 17:06:36 2013
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_DEGREERESEARCH_H
#define UI_DEGREERESEARCH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSlider>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_degreeresearchClass
{
public:
    QAction *NewComp;
    QAction *Open;
    QAction *Exit;
    QAction *Save;
    QAction *Up;
    QAction *Down;
    QAction *Left;
    QAction *Right;
    QAction *FinishCompute;
    QAction *FindVK;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QFrame *frame;
    QPushButton *stop;
    QPushButton *pause;
    QSlider *horizontalSlider;
    QGroupBox *groupBox;
    QRadioButton *ZOY;
    QRadioButton *ZOX;
    QRadioButton *XOY;
    QMenuBar *menuBar;
    QMenu *menu;
    QMenu *menu_2;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *degreeresearchClass)
    {
    if (degreeresearchClass->objectName().isEmpty())
        degreeresearchClass->setObjectName(QString::fromUtf8("degreeresearchClass"));
    degreeresearchClass->resize(749, 707);
    NewComp = new QAction(degreeresearchClass);
    NewComp->setObjectName(QString::fromUtf8("NewComp"));
    Open = new QAction(degreeresearchClass);
    Open->setObjectName(QString::fromUtf8("Open"));
    Exit = new QAction(degreeresearchClass);
    Exit->setObjectName(QString::fromUtf8("Exit"));
    Save = new QAction(degreeresearchClass);
    Save->setObjectName(QString::fromUtf8("Save"));
    Up = new QAction(degreeresearchClass);
    Up->setObjectName(QString::fromUtf8("Up"));
    Down = new QAction(degreeresearchClass);
    Down->setObjectName(QString::fromUtf8("Down"));
    Left = new QAction(degreeresearchClass);
    Left->setObjectName(QString::fromUtf8("Left"));
    Right = new QAction(degreeresearchClass);
    Right->setObjectName(QString::fromUtf8("Right"));
    FinishCompute = new QAction(degreeresearchClass);
    FinishCompute->setObjectName(QString::fromUtf8("FinishCompute"));
    FindVK = new QAction(degreeresearchClass);
    FindVK->setObjectName(QString::fromUtf8("FindVK"));
    FindVK->setEnabled(false);
    centralWidget = new QWidget(degreeresearchClass);
    centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
    gridLayout_2 = new QGridLayout(centralWidget);
    gridLayout_2->setSpacing(6);
    gridLayout_2->setMargin(11);
    gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
    gridLayout = new QGridLayout();
    gridLayout->setSpacing(6);
    gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
    frame = new QFrame(centralWidget);
    frame->setObjectName(QString::fromUtf8("frame"));
    frame->setFrameShape(QFrame::WinPanel);
    frame->setFrameShadow(QFrame::Raised);

    gridLayout->addWidget(frame, 0, 0, 1, 2);

    stop = new QPushButton(centralWidget);
    stop->setObjectName(QString::fromUtf8("stop"));
    stop->setEnabled(false);
    stop->setMaximumSize(QSize(16777215, 30));

    gridLayout->addWidget(stop, 1, 0, 1, 1);

    pause = new QPushButton(centralWidget);
    pause->setObjectName(QString::fromUtf8("pause"));
    pause->setEnabled(false);

    gridLayout->addWidget(pause, 1, 1, 1, 1);


    gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

    horizontalSlider = new QSlider(centralWidget);
    horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
    horizontalSlider->setEnabled(false);
    horizontalSlider->setMaximumSize(QSize(16777215, 20));
    horizontalSlider->setOrientation(Qt::Horizontal);

    gridLayout_2->addWidget(horizontalSlider, 1, 0, 1, 1);

    groupBox = new QGroupBox(centralWidget);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    groupBox->setMaximumSize(QSize(16777215, 50));
    ZOY = new QRadioButton(groupBox);
    ZOY->setObjectName(QString::fromUtf8("ZOY"));
    ZOY->setGeometry(QRect(20, 20, 84, 18));
    ZOY->setChecked(true);
    ZOX = new QRadioButton(groupBox);
    ZOX->setObjectName(QString::fromUtf8("ZOX"));
    ZOX->setGeometry(QRect(120, 20, 84, 18));
    XOY = new QRadioButton(groupBox);
    XOY->setObjectName(QString::fromUtf8("XOY"));
    XOY->setGeometry(QRect(210, 20, 84, 18));

    gridLayout_2->addWidget(groupBox, 2, 0, 1, 1);

    degreeresearchClass->setCentralWidget(centralWidget);
    menuBar = new QMenuBar(degreeresearchClass);
    menuBar->setObjectName(QString::fromUtf8("menuBar"));
    menuBar->setGeometry(QRect(0, 0, 749, 22));
    menu = new QMenu(menuBar);
    menu->setObjectName(QString::fromUtf8("menu"));
    menu_2 = new QMenu(menuBar);
    menu_2->setObjectName(QString::fromUtf8("menu_2"));
    degreeresearchClass->setMenuBar(menuBar);
    statusBar = new QStatusBar(degreeresearchClass);
    statusBar->setObjectName(QString::fromUtf8("statusBar"));
    degreeresearchClass->setStatusBar(statusBar);
    toolBar = new QToolBar(degreeresearchClass);
    toolBar->setObjectName(QString::fromUtf8("toolBar"));
    toolBar->setEnabled(true);
    degreeresearchClass->addToolBar(Qt::TopToolBarArea, toolBar);

    menuBar->addAction(menu->menuAction());
    menuBar->addAction(menu_2->menuAction());
    menu->addAction(NewComp);
    menu->addAction(Open);
    menu->addAction(Save);
    menu->addSeparator();
    menu->addAction(Exit);
    menu_2->addAction(Up);
    menu_2->addAction(Down);
    menu_2->addAction(Left);
    menu_2->addAction(Right);
    toolBar->addAction(NewComp);
    toolBar->addAction(Open);
    toolBar->addAction(FinishCompute);
    toolBar->addAction(Save);
    toolBar->addAction(FindVK);
    toolBar->addAction(Up);
    toolBar->addAction(Down);
    toolBar->addAction(Left);
    toolBar->addAction(Right);

    retranslateUi(degreeresearchClass);

    QMetaObject::connectSlotsByName(degreeresearchClass);
    } // setupUi

    void retranslateUi(QMainWindow *degreeresearchClass)
    {
    degreeresearchClass->setWindowTitle(QApplication::translate("degreeresearchClass", "degreeresearch", 0, QApplication::UnicodeUTF8));
    NewComp->setText(QApplication::translate("degreeresearchClass", "\320\235\320\276\320\262\321\213\320\271", 0, QApplication::UnicodeUTF8));
    Open->setText(QApplication::translate("degreeresearchClass", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", 0, QApplication::UnicodeUTF8));
    Exit->setText(QApplication::translate("degreeresearchClass", "\320\222\321\213\321\205\320\276\320\264", 0, QApplication::UnicodeUTF8));
    Save->setText(QApplication::translate("degreeresearchClass", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0, QApplication::UnicodeUTF8));
    Up->setText(QApplication::translate("degreeresearchClass", "up", 0, QApplication::UnicodeUTF8));
    Down->setText(QApplication::translate("degreeresearchClass", "down", 0, QApplication::UnicodeUTF8));
    Left->setText(QApplication::translate("degreeresearchClass", "left", 0, QApplication::UnicodeUTF8));
    Right->setText(QApplication::translate("degreeresearchClass", "right", 0, QApplication::UnicodeUTF8));
    FinishCompute->setText(QApplication::translate("degreeresearchClass", "\320\224\320\276\321\201\321\207\320\270\321\202\320\260\321\202\321\214", 0, QApplication::UnicodeUTF8));
    FindVK->setText(QApplication::translate("degreeresearchClass", "\320\235\320\260\320\271\321\202\320\270 \320\222\320\232", 0, QApplication::UnicodeUTF8));
    stop->setText(QApplication::translate("degreeresearchClass", "\320\241\321\202\320\276\320\277", 0, QApplication::UnicodeUTF8));
    pause->setText(QApplication::translate("degreeresearchClass", "\320\237\320\260\321\203\320\267\320\260", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("degreeresearchClass", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213", 0, QApplication::UnicodeUTF8));
    ZOY->setText(QApplication::translate("degreeresearchClass", "ZOY", 0, QApplication::UnicodeUTF8));
    ZOX->setText(QApplication::translate("degreeresearchClass", "ZOX", 0, QApplication::UnicodeUTF8));
    XOY->setText(QApplication::translate("degreeresearchClass", "XOY", 0, QApplication::UnicodeUTF8));
    menu->setTitle(QApplication::translate("degreeresearchClass", "\320\244\320\260\320\271\320\273", 0, QApplication::UnicodeUTF8));
    menu_2->setTitle(QApplication::translate("degreeresearchClass", "\320\234\320\260\321\201\321\210\321\202\320\260\320\261", 0, QApplication::UnicodeUTF8));
    toolBar->setWindowTitle(QApplication::translate("degreeresearchClass", "toolBar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class degreeresearchClass: public Ui_degreeresearchClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEGREERESEARCH_H
