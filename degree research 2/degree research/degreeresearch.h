#ifndef DEGREERESEARCH_H
#define DEGREERESEARCH_H

#include <QtGui/QMainWindow>
#include "ui_degreeresearch.h"
#include <QCloseEvent>
#include <QFileDialog>
#include <QMessageBox>
#include "param.h"
#include "countstepsfinishcomp.h"
#include "DiagramPainter.h"
#include <QTimer>
#include <ActiveQt/qaxobject.h>
#include <ActiveQt/qaxbase.h>

#include "stdafx.h"
#include "../RK2_SC_GPU_VA_Lag_Diod/c_RK2_SC_GPU_VA_Lag_Diod.h"
#include "../Viewer/c_Viewer.h"

class degreeresearch : public QMainWindow
{
	Q_OBJECT

public:
	degreeresearch(QWidget *parent = 0, Qt::WFlags flags = 0);
	~degreeresearch();

	HWND GetHWNDAreaDrawing(){return ui.frame->winId();}
	static void StartRender(bool _t);

	static HANDLE m_ThreadUpdate;

private:
	Ui::degreeresearchClass ui;

	static DWORD WINAPI ThreadUpdate( LPVOID lpParam );

	static DiagramPainter* m_Painter;
	static c_ICompute* m_Compute;

	static QTimer* m_Timer;

	static bool m_ComputeStarted;

	VECTOR3 m_Range;

	static int idArrayOfPoints;

private slots:

	void on_NewComp_triggered();
	void on_Open_triggered();
	void on_Save_triggered();

	void on_Up_triggered();
	void on_Down_triggered();
	void on_Left_triggered();
	void on_Right_triggered();
	void on_FinishCompute_triggered();
	void on_FindVK_triggered();

	void on_horizontalSlider_valueChanged(int i);

	void closeEvent(QCloseEvent *event);
	void on_stop_clicked();
	void on_pause_clicked();

	void on_ZOX_toggled(bool);
	void on_ZOY_toggled(bool);
	void on_XOY_toggled(bool);

	void updateRenderInfo();
};

#endif // DEGREERESEARCH_H
