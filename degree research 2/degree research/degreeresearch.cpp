#include "degreeresearch.h"

HANDLE degreeresearch::m_ThreadUpdate;

DiagramPainter* degreeresearch::m_Painter;
c_ICompute* degreeresearch::m_Compute;

QTimer* degreeresearch::m_Timer;
bool degreeresearch::m_ComputeStarted = false;
int degreeresearch::idArrayOfPoints = -1;

degreeresearch::degreeresearch(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	ui.setupUi(this);
	m_Compute = 0;

	this->m_Painter = new DiagramPainter(this, this->ui.frame);

	m_Timer = new QTimer(this);
	m_Timer->setSingleShot(false);
	connect(m_Timer, SIGNAL(timeout()), this, SLOT(updateRenderInfo()));

	connect(ui.FinishCompute, SIGNAL(triggered()), this, SLOT(on_actionFinishCompute()));

	connect(ui.horizontalSlider, SIGNAL(valueChanged()), this, SLOT(on_stepChanged()));

	idArrayOfPoints = -1;

	ui.Save->setEnabled(false);
}
degreeresearch::~degreeresearch()
{	
}
//////////  ���������� 
void degreeresearch::on_ZOX_toggled(bool _t)
{
	if(_t && idArrayOfPoints > -1)
	{
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setProjectPlane(ZOX);
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.x, m_Range.x);

		m_Painter->clearLines();
		m_Painter->addLine(m_Compute->paramColculate()->sizeZ, false, 0, m_Range.z);
		//m_Painter->SetProjectPlane(ZOX);
		//m_Painter->SetPaintRange(m_Range.x, 0.0, m_Range.z);
		update();
	}
}
void degreeresearch::on_ZOY_toggled(bool _t)
{
	if(_t && idArrayOfPoints > -1)
	{
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setProjectPlane(ZOY);
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.y, m_Range.y);

		m_Painter->clearLines();
		m_Painter->addLine(m_Compute->paramColculate()->sizeZ, false, 0, m_Range.z);
		//m_Painter->SetProjectPlane(ZOY);
		//m_Painter->SetPaintRange(m_Range.y, 0.0, m_Range.z);
		update();
	}
}
void degreeresearch::on_XOY_toggled(bool _t)
{	
	if(_t && idArrayOfPoints > -1)
	{
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setProjectPlane(XOY);
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect( -m_Range.y, m_Range.y, -m_Range.x, m_Range.x);

		m_Painter->clearLines();
		//m_Painter->SetProjectPlane(XOY);
		//m_Painter->SetPaintRange(m_Range.y, -m_Range.x, m_Range.x);
		update();
	}
}
void degreeresearch::on_Up_triggered()
{
	if(idArrayOfPoints < 0) return;
	if (ui.XOY->isChecked())
	{
		m_Range.y = m_Range.y*0.9;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect( -m_Range.y, m_Range.y, -m_Range.x, m_Range.x);
	}else if (ui.ZOX->isChecked())
	{
		m_Range.x = m_Range.x*0.9;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.x, m_Range.x);
	}else if (ui.ZOY->isChecked())
	{
		m_Range.y = m_Range.y*0.9;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.y, m_Range.y);
	}
	update();
}
void degreeresearch::on_Down_triggered()
{
	if(idArrayOfPoints < 0) return;
	if (ui.XOY->isChecked())
	{
		m_Range.y = m_Range.y*1.1;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect( -m_Range.y, m_Range.y, -m_Range.x, m_Range.x);
	}else if (ui.ZOX->isChecked())
	{
		m_Range.x = m_Range.x*1.1;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.x, m_Range.x);
	}else if (ui.ZOY->isChecked())
	{
		m_Range.y = m_Range.y*1.1;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.y, m_Range.y);
	}
	update();
}
void degreeresearch::on_Left_triggered()
{
	if(idArrayOfPoints < 0) return;
	if (ui.XOY->isChecked())
	{
		m_Range.x = m_Range.x*0.9;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect( -m_Range.y, m_Range.y, -m_Range.x, m_Range.x);
	}else if (ui.ZOX->isChecked())
	{
		m_Range.z = m_Range.z*0.9;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.x, m_Range.x);
	}else if (ui.ZOY->isChecked())
	{
		m_Range.z = m_Range.z*0.9;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.y, m_Range.y);
	}
	update();
}
void degreeresearch::on_Right_triggered()
{
	if(idArrayOfPoints < 0) return;
	if (ui.XOY->isChecked())
	{
		m_Range.x = m_Range.x*1.1f;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect( -m_Range.y, m_Range.y, -m_Range.x, m_Range.x);
	}else if (ui.ZOX->isChecked())
	{
		m_Range.z = m_Range.z*1.1f;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.x, m_Range.x);
	}else if (ui.ZOY->isChecked())
	{
		m_Range.z = m_Range.z*1.1f;
		m_Painter->getPointsAtIndex(idArrayOfPoints)->setRect(0.0f, m_Range.z, -m_Range.y, m_Range.y);
	}
	update();
}
void degreeresearch::on_horizontalSlider_valueChanged(int _step)
{
	vector< VECTOR4 > points;

	for(int i = 0; i < (*m_Compute->Shift())[_step]; i++)
		if( (*m_Compute->Activity())[_step][i] )
			points.insert(points.end(), (*m_Compute->Coordinate())[_step][i]);

	PointsArray3D tmp( points, QColor(255, 0, 0), ZOY, true, true);
	tmp.setRect( 0.0f, m_Range.z, -m_Range.y, m_Range.y);

	m_Painter->clear3DPointsArrays();
	idArrayOfPoints = m_Painter->add3DPoints( tmp );

	ui.statusBar->showMessage("Curent Steps: " + QString::number(_step) + ", Count Particls: " + QString::number((*m_Compute->Shift())[_step]));
	update();
}
void degreeresearch::updateRenderInfo()
{
	if(m_Compute->curentCountSteps() == 0)
		return;

	int _step = m_Compute->curentCountSteps()-1;
	int _countPart = m_Compute->countParticleAddedAtStap()*_step;
	vector< VECTOR4 > points;

	for(int i = 0; i < _countPart; i++)
	{
		if( (*m_Compute->Activity())[_step][i] )
			points.insert(points.end(), (*m_Compute->Coordinate())[_step][i]);
	}

	PointsArray3D tmp(points, QColor(255, 0, 0), ZOY, true, true);
	tmp.setRect( 0.0f, m_Range.z, -m_Range.y, m_Range.y);

	m_Painter->clear3DPointsArrays();
	idArrayOfPoints = m_Painter->add3DPoints( tmp );

	ui.statusBar->showMessage("Curent Steps: " + QString::number(_step+1) + ", Count Particls: " + QString::number(m_Compute->curentCountParticls()) + ", Compute time: " + QString::number(m_Compute->computeTime()));

	if(m_Compute->Computed())
	{
		m_Timer->stop();
		ui.Save->setEnabled(true);
		ui.FindVK->setEnabled(true);
		ui.pause->setEnabled(false);
		ui.stop->setEnabled(false);
	}

	update();
}
void degreeresearch::on_NewComp_triggered()
{
	if(m_ComputeStarted)
		on_stop_clicked();

	if(m_Compute)
		delete m_Compute;

	s_paramColculate _paramColculate;
	param dialog(this, &_paramColculate);
	if(dialog.exec() == QDialog::Accepted)
	{
		ui.horizontalSlider->setEnabled(false);

		m_Compute = new c_RK2_SC_GPU_VA_Lag_Diod();
		m_Compute->Create(_paramColculate);


		m_Range.x = m_Compute->RangeX();
		m_Range.y = m_Compute->RangeY();
		m_Range.z = m_Compute->RangeZ();

		m_Painter->addLine(_paramColculate.sizeZ, false, 0, m_Range.z);
		//m_Painter->SetProjectPlane(ZOY);
		//m_Painter->SetPaintRange(m_Range.y, 0.0, m_Range.z);
		update();

		m_Timer->start(1000);
		ui.pause->setEnabled(true);
		ui.stop->setEnabled(true);
		StartRender(true);
	}
}
void degreeresearch::on_Open_triggered()
{
	if(m_ComputeStarted)
		on_stop_clicked();

	if(m_Compute)
		delete m_Compute;	

	string nameFile = QFileDialog::getOpenFileName(this, "�������", QString(),"����� ������(*.dat)").toStdString();
	
	m_Compute = new c_Viewer();
	m_Compute->Load(nameFile);

	m_Range.x = m_Compute->RangeX();
	m_Range.y = m_Compute->RangeY();
	m_Range.z = m_Compute->RangeZ();

	//m_Painter->SetProjectPlane(ZOY);
	//m_Painter->SetPaintRange(m_Range.y, 0.0, m_Range.z);
	update();

	ui.horizontalSlider->setMaximum(m_Compute->countSteps()-1);
	ui.horizontalSlider->setEnabled(true);

	ui.pause->setEnabled(true);
	ui.stop->setEnabled(true);
	ui.FindVK->setEnabled(true);
}
void degreeresearch::on_Save_triggered()
{
	string nameFile = QFileDialog::getSaveFileName(this, "�������", QString(),"����� ������(*.dat)").toStdString();
	Save(nameFile, m_Compute);
}
void degreeresearch::on_FinishCompute_triggered()
{
	if(m_ComputeStarted)
		on_stop_clicked();

	if(m_Compute)
		delete m_Compute;

	string nameFile = QFileDialog::getOpenFileName(this, "�������", QString(),"����� ������(*.dat)").toStdString();

	unsigned int countSteps;
	if(nameFile != "")
	{
		countStepsFinishComp dialog(this, &countSteps);
		if(dialog.exec() == QDialog::Accepted)
		{
			string _nameParFile = nameFile;
			_nameParFile.erase(_nameParFile.size()-3,3);
			_nameParFile = _nameParFile + "par";

			ifstream inoutParFile(_nameParFile.c_str(), ios::in | ios::binary );

			s_fullParamColculate fullParamColculate;

			inoutParFile.read((char*)&fullParamColculate, sizeof(s_fullParamColculate));

			inoutParFile.close();

			fullParamColculate.countSteps = countSteps;

			ui.horizontalSlider->setEnabled(false);
			
			m_Compute = new c_RK2_SC_GPU_VA_Lag_Diod();
			m_Compute->Load(nameFile);
			m_Compute->Create(fullParamColculate);

			m_Range.x = m_Compute->RangeX();
			m_Range.y = m_Compute->RangeY();
			m_Range.z = m_Compute->RangeZ();

			//m_Painter->SetProjectPlane(ZOY);
			//m_Painter->SetPaintRange(m_Range.y, 0.0, m_Range.z);
			update();

			m_Timer->start(1000);
			ui.pause->setEnabled(true);
			ui.stop->setEnabled(true);
			ui.FindVK->setEnabled(true);
			StartRender(true);
		}
	}
}
void degreeresearch::on_FindVK_triggered()
{
	// �������� ��������� �� Excel
	QAxObject *mExcel = new QAxObject( "Excel.Application",this); 
	// �� �����
	QAxObject *workbooks = mExcel->querySubObject( "Workbooks" ); 
	// �� ����������, ������ ������� �����
	QAxObject *workbook = workbooks->querySubObject( "Add()" ); 
	// �� ����� (����� �������)
	QAxObject *mSheets = workbook->querySubObject( "Sheets" ); 
	// ���������, ����� ���� �������. � ���� �� ���������� topic.
	QAxObject *StatSheet = mSheets->querySubObject( "Item(const QVariant&)", QVariant("����1") );

	//m_Painter->clearDiagram();
	/////////// ������ ���������� ///////////////////////////////
	int stepNumber = ui.horizontalSlider->value();
	if( stepNumber == 0)
		stepNumber = m_Compute->countSteps() - 1;

	int countSteps = 80;
	float step = m_Compute->paramColculate()->sizeZ/countSteps;

	float* m_U = new float[countSteps];


	float maxU = computeUWithoutLagField(	(*m_Compute->Coordinate()),
											(*m_Compute->Activity()),
											(*m_Compute->Shift())[stepNumber],
											stepNumber,
											m_U,
											m_Compute->k(),
											m_Range.x*1.5f,
											m_Range.y*1.5f,
											m_Compute->paramColculate()->sizeZ,
											step,
											countSteps);

	vector<float> points;
	for(int i = 0; i < countSteps; i++)
		points.insert(points.end(), m_U[i] );

	//Diagram d(points, 0, m_Range.z*1.2f, step*1.2f, QColor(0,0,0),"���������", true);

	//m_Painter->addDiagram(d);

	delete[] m_U;

	///////////////////////////////

	vector<float> pointsVK;

	float* positiveCurrent = new float[countSteps];

	////////////////////////////// ������ ���
	for(int i = 0; i < countSteps; i++)
		positiveCurrent[i] = 0;

	computeCurrent(	(*m_Compute->Coordinate())[stepNumber], 
					(*m_Compute->Velocity())[stepNumber], 
					(*m_Compute->Activity())[stepNumber], 
					(*m_Compute->Shift())[stepNumber], 
					positiveCurrent, 
					countSteps, 
					m_Compute->k(), 
					m_Compute->paramColculate()->sizeZ/(float)countSteps,
					2);

	for(int i = 0; i < countSteps; i++)
		pointsVK.insert(	pointsVK.end(),	positiveCurrent[i] );

	//Diagram d1(pointsVK, 0, m_Range.z, m_Range.z/(float)countPoint, QColor(50,150,255),"������ ���", true);

	//m_Painter->addDiagram(d1);


	/////////////////////////////// �������� ���

	vector<float> pointsVK2;

	for(int i = 0; i < countSteps; i++)
		positiveCurrent[i] = 0;

	computeCurrent(	(*m_Compute->Coordinate())[stepNumber], 
					(*m_Compute->Velocity())[stepNumber], 
					(*m_Compute->Activity())[stepNumber], 
					(*m_Compute->Shift())[stepNumber], 
					positiveCurrent, 
					countSteps, 
					m_Compute->k(), 
					m_Compute->paramColculate()->sizeZ/(float)countSteps,
					3);

	for(int i = 0; i < countSteps; i++)
		pointsVK2.insert(	pointsVK2.end(),	positiveCurrent[i] );

	//Diagram d2(pointsVK, 0, m_Range.z, m_Range.z/(float)countPoint, QColor(50,150,255),"�������� ���", true);

	//m_Painter->addDiagram(d2);

	//////////////////////////////

	delete[] positiveCurrent;

	QAxObject* cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, 1);
	// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
	cell->setProperty("Value", QVariant("����������"));
	// ������������ ������
	delete cell;

	cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, 2);
	// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
	cell->setProperty("Value", QVariant("��������� �� ���"));
	// ������������ ������
	delete cell;

	cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, 3);
	// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
	cell->setProperty("Value", QVariant("������ ���"));
	// ������������ ������
	delete cell;

	cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", 1, 4);
	// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
	cell->setProperty("Value", QVariant("�������� ���"));
	// ������������ ������
	delete cell;

	for(int i = 0; i < countSteps; i++)
	{
		cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, 1);
		// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
		cell->setProperty("Value", QVariant(i*m_Compute->paramColculate()->sizeZ/(float)countSteps));
		// ������������ ������
		delete cell;

		cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, 2);
		// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
		cell->setProperty("Value", QVariant(-points[i]));
		// ������������ ������
		delete cell;

		cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, 3);
		// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
		cell->setProperty("Value", QVariant(pointsVK[i]));
		// ������������ ������
		delete cell;

		cell = StatSheet->querySubObject("Cells(QVariant,QVariant)", i+2, 4);
		// ������� �������� ���������� data (����� ���, ���������� � QVariant) � ���������� ������
		cell->setProperty("Value", QVariant(pointsVK2[i]));
		// ������������ ������
		delete cell;
	}
		
	mExcel->setProperty("Visible", true);

	update();
}
void degreeresearch::closeEvent(QCloseEvent *event)
{
	StartRender( false );

	m_Timer->stop();
	delete m_Timer;

	if(m_Compute)
		delete m_Compute;
}
void degreeresearch::StartRender(bool _t)
{
	if(_t)
	{
		m_ThreadUpdate = CreateThread(
							NULL,              // default security attributes
							0,                 // use default stack size  
							ThreadUpdate,          // thread function 
							NULL,             // argument to thread function 
							NULL,                 // use default creation flags // CREATE_SUSPENDED
							NULL);   // returns the thread identifier 
	} else 
	{
		if(m_ThreadUpdate)
		{
			m_Timer->stop();
			SuspendThread(m_ThreadUpdate);
			TerminateThread(m_ThreadUpdate, NULL);
			m_ComputeStarted = false;
		}
	}
}
void degreeresearch::on_pause_clicked()
{
	static bool t = false;
	if(!t)
	{
		SuspendThread(m_ThreadUpdate);

		ui.pause->setText("����������");
		t = true;
	}else{
		ResumeThread(m_ThreadUpdate);

		ui.pause->setText("�����");
		t = false;
	}
}
void degreeresearch::on_stop_clicked()
{
	int t = QMessageBox::warning(	0,
									"",
									"������ ����� �������. \n�� �������?",
									"��",
									"���",
									QString::null,
									0,
									1);
	if(t)
		return;

	ui.pause->setEnabled(false);
	ui.stop->setEnabled(false);
	StartRender(false);
	m_Painter->clear3DPointsArrays();
	m_Painter->clearDiagram();
	ui.statusBar->showMessage("");
	update();
}

DWORD WINAPI degreeresearch::ThreadUpdate( LPVOID lpParam )
{
	m_ComputeStarted = true;
	m_Compute->StartCompute();

	char buf[20];
	sprintf(buf,"%d ���",m_Compute->computeTime());
	MessageBoxA( 0, buf, "����� �������", 0);

	m_ComputeStarted = false;

	return 0;
}