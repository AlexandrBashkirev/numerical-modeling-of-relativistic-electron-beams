#ifndef VKCOMPUTEMETHOD_H
#define VKCOMPUTEMETHOD_H

#include <QWidget>
#include "ui_vkcomputemethod.h"

class VKComputeMethod : public QWidget
{
	Q_OBJECT

public:
	VKComputeMethod(QWidget *parent = 0);
	~VKComputeMethod();

private:
	Ui::VKComputeMethodClass ui;
};

#endif // VKCOMPUTEMETHOD_H
