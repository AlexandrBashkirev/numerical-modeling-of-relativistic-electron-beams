#include "param.h"

param::param(QWidget *parent, s_paramColculate* tmp)
	: QDialog(parent)
{
	tmpParam = tmp;
	ui.setupUi(this);

	SetParam();
}
param::~param()
{
}

void param::on_ok_clicked()
{
	tmpParam->E = ui.E->value();
	tmpParam->B = ui.B->value();
	tmpParam->V = ui.V->value();

	tmpParam->directionE = VECTOR3(ui.Ex->value(), ui.Ey->value(), ui.Ez->value());
	tmpParam->directionE.norm();

	tmpParam->directionB = VECTOR3(ui.Bx->value(), ui.By->value(), ui.Bz->value());
	tmpParam->directionB.norm();

	tmpParam->directionStartV = VECTOR3(ui.Vx->value(), ui.Vy->value(), ui.Vz->value());
	tmpParam->directionStartV.norm();

	tmpParam->T = (double)ui.valueT->value()*pow((double)10.0, -ui.powerT->value());
	tmpParam->countSteps = ui.countSteps->value();

	tmpParam->Density = ui.Density->value();

	tmpParam->k = ui.k->value()*2;

	if(ui.squere->isChecked())
	{
		tmpParam->sizeCathodeX = ui.sizeCathodeX->value();
		tmpParam->sizeCathodeY = ui.sizeCathodeY->value();
	}
	else
	{
		tmpParam->sizeCathodeX = ui.sizeCathodeR->value();
		tmpParam->sizeCathodeY = -1;
	}

	tmpParam->sizeZ = ui.sizeZ->value();

	tmpParam->y0 = ui.y0->value();

	this->accept();
	this->close();
}
void param::on_close_clicked()
{
	this->reject();
	this->close();
}
void param::on_Density_valueChanged()
{
	SetParam();
}
void param::on_squere_toggled(bool t)
{
	SetParam();
}
void param::on_circle_toggled(bool t)
{
	SetParam();
}
void param::on_sizeCathodeX_valueChanged()
{
	SetParam();
}
void param::on_sizeCathodeY_valueChanged()
{
	SetParam();
}
void param::on_sizeCathodeR_valueChanged()
{
	SetParam();
}
void param::on_countSteps_valueChanged()
{
	SetParam();
}
void param::on_k_valueChanged()
{
	SetParam();
}
void param::on_Vz_valueChanged()
{
	SetParam();
}
void param::on_V_valueChanged()
{
	SetParam();
}
void param::on_y0_valueChanged()
{
	SetParam();
}
void param::SetParam()
{
	double e = -1.602e-19;		// ����� ���������
	double c = 2.99792458e+8;	// �������� �����
	double m = 9.11e-31;		// ����� ���������
	double m0 = 1.25663706e-6;	// ��������� ����������
	double e0 = 8.8541878e-12;	// ��������������� ������������� ������

	float S;

	if(ui.squere->isChecked())
		S = ui.sizeCathodeX->value()*ui.sizeCathodeY->value();
	else
		S = 3.1415f*ui.sizeCathodeR->value()*ui.sizeCathodeR->value();

	unsigned int k = ui.k->value()*2;
	float t = (double)ui.valueT->value()*pow((double)10.0, -ui.powerT->value());
	float tmp = fabs(S*ui.Vz->value()*ui.V->value()*t*ui.Density->value()/(e*k));
	ui.particlePerStep->setText(QString::number((int)(tmp)));
	ui.maxParticle->setText(QString::number((int)(tmp)*ui.countSteps->value()));
	ui.streamLength->setText(QString::number((int)ui.Vz->value()*ui.V->value()*t*ui.countSteps->value()));

	
	float i = ui.Vz->value()*ui.V->value()*ui.Density->value()*S;
	ui.I0->setText(QString::number((int)i));
	float u = fabs(m/e)*ui.Vz->value()*ui.Vz->value()*ui.V->value()*ui.V->value()/2.0f;
	float p = i/pow(u, 1.5f);
	ui.perveance->setText(QString::number(p));

	// ��� �������� �����������
	float _a = 1.0f/( 4.0f*e0*ui.sizeCathodeX->value()*sqrt(2.0f*fabs(e/m)) );
	ui.Zkr->setText(QString::number(tan(ui.y0->value()*PI/180)/(p*_a)));
}