#ifndef DIAGRAMPAINTER_H
#define DIAGRAMPAINTER_H

#include <QWidget>
#include <QFrame>
#include "QPainter"
#include "QColor"
#include <QMessageBox>
#include "QPaintEvent"
#include <QString>

#include "MyPoint.h"
#include "../CoreUtilit/CoreUtilit.h"

enum e_ProjectPlane
{
	ZOX,
	ZOY,
	XOY,
};
struct Diagram
{
	vector<float> points;
	float step;
	QColor color; 
	string name;
	float minX;
	float maxX;
	float minY;
	float maxY;
	bool vertAxis;
	bool horizAxis;

	Diagram(vector<float> _points, float _minX, float _maxX, float _step, QColor _color, string _name, bool _vertAxis = false, bool _horizAxis = false)
	{
		vertAxis = _vertAxis;
		horizAxis = _horizAxis;
		points = _points;
		step = _step;
		color = _color;
		name = _name;
		minX = _minX;
		maxX = _maxX;

		minY = 100000.0f;
		maxY = -100000.0f;

		for(int p = 0; p < points.size(); p++)
		{
			if(minY > points[p])
				minY = points[p];
			if(maxY < points[p])
				maxY = points[p];
		}
		maxY *= 1.1f;
	}
};

struct PointsArray3D
{
	vector<VECTOR4>  points;
	QColor color; 
	float minX;
	float maxX;
	float minY;
	float maxY;
	DWORD projectPlane;
	bool vertAxis;
	bool horizAxis;

	PointsArray3D( vector<VECTOR4> _points, QColor _color, DWORD _projectPlane, bool _vertAxis = false, bool _horizAxis = false )
	{
		vertAxis = _vertAxis;
		horizAxis = _horizAxis;

		points = _points;
		color = _color;

		projectPlane = _projectPlane;
	}
	void setProjectPlane(DWORD _t){projectPlane = _t;}
	void setRect(float _minX, float _maxX, float _minY, float _maxY)
	{
		minX = _minX;
		maxX = _maxX;
		minY = _minY;
		maxY = _maxY;
	}
};

struct LineInfo
{
	float coord;
	bool isHorizontal;
	float min;
	float max;
};

class DiagramPainter : public QWidget
{
	Q_OBJECT

public:
	DiagramPainter( QWidget *parent, QFrame *_frame);
	~DiagramPainter();
	
	int add3DPoints(PointsArray3D points);
	PointsArray3D* getPointsAtIndex(int id);
	void clear3DPointsArrays();

	void addDiagram(Diagram d);
	void clearDiagram();

	void addLine(float coord, bool isHorizontal, float min, float max);
	void clearLines();

	void lockDrawing();
	void unlockDrawing();

private:

	vector<LineInfo> lines;
	vector<Diagram> diagrams;
	vector< PointsArray3D > pointsArrays;

	QFrame *m_frame;

	CRITICAL_SECTION	m_criticalSectionUpdate;

	void paintEvent ( QPaintEvent * e );
	void resizeEvent ( QResizeEvent * e );
protected:
};

#endif // DIAGRAMPAINTER_H
