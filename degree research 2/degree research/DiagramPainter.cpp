#include "DiagramPainter.h"
#include "transformation.h"

DiagramPainter::DiagramPainter(QWidget *parent, QFrame *_frame)
	: QWidget(parent)
{
	m_frame = _frame;

	InitializeCriticalSection((LPCRITICAL_SECTION)&m_criticalSectionUpdate);
}
DiagramPainter::~DiagramPainter()
{
}
	///////// 3D �����
int DiagramPainter::add3DPoints(PointsArray3D points )
{
	EnterCriticalSection(&m_criticalSectionUpdate);
	pointsArrays.insert(pointsArrays.end(), points);
	LeaveCriticalSection(&m_criticalSectionUpdate);	
	update();

	return pointsArrays.size() - 1;
}
void DiagramPainter::clear3DPointsArrays()
{
	EnterCriticalSection(&m_criticalSectionUpdate);
	pointsArrays.clear();
	LeaveCriticalSection(&m_criticalSectionUpdate);	
	update();
}
PointsArray3D* DiagramPainter::getPointsAtIndex(int id)
{
	return &pointsArrays[id];
}
	///////  ���������
void DiagramPainter::addDiagram(Diagram d)
{
	EnterCriticalSection(&m_criticalSectionUpdate);
	diagrams.insert(diagrams.end(), d);
	LeaveCriticalSection(&m_criticalSectionUpdate);	
	update();
}
void DiagramPainter::clearDiagram()
{
	EnterCriticalSection(&m_criticalSectionUpdate);
	diagrams.clear();
	LeaveCriticalSection(&m_criticalSectionUpdate);	
	update();
}
void DiagramPainter::lockDrawing()
{
	EnterCriticalSection(&m_criticalSectionUpdate);
}
void DiagramPainter::unlockDrawing()
{
	LeaveCriticalSection(&m_criticalSectionUpdate);
}
void DiagramPainter::addLine(float coord, bool isHorizontal, float min, float max)
{
	LineInfo lineInfo;
	lineInfo.coord = coord;
	lineInfo.isHorizontal = isHorizontal;
	lineInfo.min = min;
	lineInfo.max = max;

	EnterCriticalSection(&m_criticalSectionUpdate);
	lines.push_back(lineInfo);
	LeaveCriticalSection(&m_criticalSectionUpdate);	
	update();
}
void DiagramPainter::clearLines()
{
	EnterCriticalSection(&m_criticalSectionUpdate);
	lines.clear();
	LeaveCriticalSection(&m_criticalSectionUpdate);	
	update();
}
	/// �������
void DiagramPainter::paintEvent ( QPaintEvent * e )
{
	//if(m_criticalSectionUpdate.LockCount > 0)return;
	EnterCriticalSection(&m_criticalSectionUpdate);
	////  �������� ������� � ������ ������� ��������� ��������� �������
	int height = m_frame->height();
	int width = m_frame->width();

	this->setGeometry(m_frame->x(), m_frame->y()+45, width, height);

	int X, Y;
	for(int i = 0; i < pointsArrays.size(); i++)
	{
		PointsArray3D p = pointsArrays[i];

		s_MyPoint tmp(0.0,0.0, p.color, 0);

		for(int j = 0; j < p.points.size(); j++)
		{
			switch (p.projectPlane)
			{
			case ZOX:
				X = ValueToPixel(p.points[j].z, p.minX, p.maxX, width);
				Y = ValueToPixel(p.points[j].x, p.maxY, p.minY, height);
				break;
			case ZOY:
				X = ValueToPixel(p.points[j].z, p.minX, p.maxX, width);
				Y = ValueToPixel(p.points[j].y, p.maxY, p.minY, height);
				break;
			case XOY:
				X = ValueToPixel(p.points[j].x, p.minX, p.maxX, width);
				Y = ValueToPixel(p.points[j].y, p.maxY, p.minY, height);
				break;
			}
			
			if((X < 1000)&&(X > 0)&&(Y < 1000)&&(Y > -1000))
				tmp.DrawPoint(this, X, Y);
		}
	}
		////  ������ ��� ���������
	QPainter painter(this);

	painter.setPen(QColor(0,0,0));
	painter.drawLine ( 0, height - 2, width, height - 2 );
	painter.drawLine ( 2, 0, 2, height );
	for(int i = -2; i <= 2; i++)
	{
		painter.drawLine ( width/2 + i*width/6, height - 10, width/2 + i*width/6, height );
		painter.drawLine ( 0, height/2 + i*height/6, 5, height/2 + i*height/6 );
	}

	//////// lines 

	for(int i = 0; i < lines.size(); i++)
	{
		if(lines[i].isHorizontal)
		{
			float t = ValueToPixel(lines[i].coord, lines[i].max, lines[i].min, height);
			painter.drawLine ( 0, t, width, t );
		}
		else
		{
			float t = ValueToPixel(lines[i].coord, lines[i].min, lines[i].max, width);
			painter.drawLine ( t, 0, t, height );
		}
	}
	
	// ������� � ����
	painter.setFont ( QFont("Arial", 10) );

	for(int i = -2; i <= 2; i++)
	{
		painter.setPen(QColor(0, 0, 0));

		QString tmpStr;
		double t;
		
		for(int j = 0; j < pointsArrays.size(); j++)
		{
			PointsArray3D p = pointsArrays[j];

			if(p.horizAxis)
			{
				t = (double)(p.minX + (i+3)*(fabs(p.minX) + fabs(p.maxX))/6);
				tmpStr = QString::number(t);
				if(t < 0.0)
					tmpStr.resize(7);
				else if(tmpStr.length() > 6)
					tmpStr.resize(6);

				painter.drawText ( width/2 + i*width/6, height - 10, tmpStr);
			}

			if(p.vertAxis)
			{
				t = (double)(p.maxY - (i+3)*(fabs(p.minY) + fabs(p.maxY))/6);
				tmpStr = QString::number(t);
				if(t < 0.0)
					tmpStr.resize(7);
				else if(tmpStr.length() > 6)
					tmpStr.resize(6);

				painter.drawText ( 10, height/2 + i*height/6, tmpStr );
			}
		}

		for(int j = 0; j < diagrams.size(); j++)
		{
			Diagram d = diagrams[j];

			painter.setPen(d.color);

			if(d.horizAxis)
			{
				double t = (double)(d.minX + (i+3)*(fabs(d.minX) + fabs(d.maxX))/6);
				tmpStr = QString::number(t);
				if(t < 0.0)
					tmpStr.resize(7);
				else if(tmpStr.length() > 6)
					tmpStr.resize(6);

				painter.drawText ( width/2 + i*width/6, height - 10 - 20*(j+1), tmpStr);
			}

			if(d.vertAxis)
			{
				t = (double)(d.maxY - (i+3)*(fabs(d.minY) + fabs(d.maxY))/6);
				tmpStr = QString::number(t);
				if(t < 0.0)
					tmpStr.resize(7);
				else if(tmpStr.length() > 6)
					tmpStr.resize(6);

				painter.drawText ( 10, height/2 + i*height/6  + 20*(j+1), tmpStr );
			}

		}
	}

	// ������ �������
	for(int i = 0; i < diagrams.size(); i++)
	{
		Diagram d = diagrams[i];

		painter.setPen(d.color);

		for(int p = 0; p < d.points.size()-1; p++)
		{
			int x1 = ValueToPixel(p*d.step, d.minX, d.maxX, width);
			int y1 = height - ValueToPixel(d.points[p], d.minY, d.maxY, height);

			int x2 = ValueToPixel((p+1)*d.step, d.minX, d.maxX, width);
			int y2 = height - ValueToPixel(d.points[p+1], d.minY, d.maxY, height);

			painter.drawLine ( x1, y1, x2, y2 );
		}
	}

	LeaveCriticalSection(&m_criticalSectionUpdate);
}
void DiagramPainter::resizeEvent ( QResizeEvent * e )
{
	update();
}