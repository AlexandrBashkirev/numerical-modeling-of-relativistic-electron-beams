#ifndef PARAM_H
#define PARAM_H

#include <QDialog>
#include "ui_param.h"

#include "stdafx.h"

class param : public QDialog
{
	Q_OBJECT

public:
	param(QWidget *parent = 0, s_paramColculate* tmp = 0);
	~param();

private:
	s_paramColculate* tmpParam;
	Ui::paramClass ui;

	void SetParam();

private slots:
	void on_ok_clicked();
	void on_close_clicked();

	void on_Density_valueChanged();
	void on_sizeCathodeX_valueChanged();
	void on_sizeCathodeY_valueChanged();
	void on_sizeCathodeR_valueChanged();
	void on_countSteps_valueChanged();
	void on_k_valueChanged();
	void on_Vz_valueChanged();
	void on_V_valueChanged();
	void on_y0_valueChanged();

	void on_squere_toggled(bool t);
	void on_circle_toggled(bool t);
};

#endif // PARAM_H
