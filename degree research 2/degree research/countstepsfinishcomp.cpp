#include "countstepsfinishcomp.h"

countStepsFinishComp::countStepsFinishComp(QWidget *parent, unsigned int * t)
	: QDialog(parent)
{
	ui.setupUi(this);
	countSteps = t;
}

countStepsFinishComp::~countStepsFinishComp()
{

}
void countStepsFinishComp::on_ok_clicked()
{
	*countSteps = ui.countSteps->value();

	this->accept();
	this->close();
}
void countStepsFinishComp::on_close_clicked()
{
	this->reject();
	this->close();
}