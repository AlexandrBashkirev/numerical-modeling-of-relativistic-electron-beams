#ifndef _MYPOINT_
#define _MYPOINT_

#include "QPainter"
#include "QColor"
#include <QMessageBox>

struct s_MyPoint
{
public:
	float m_x;
	float m_y;
	QColor m_color;
	int m_id;


	~s_MyPoint();
	s_MyPoint();
	s_MyPoint(float _x, float _y, QColor _color, int _id);
	void DrawPoint(QWidget* _widget, int dx, int dy);

	float GetX();
	float GetY();

	s_MyPoint operator+(s_MyPoint &t_Vector);
	s_MyPoint operator-(s_MyPoint &t_Vector);
	s_MyPoint operator*(const float &t);
	s_MyPoint operator/(const float &t);
};
#endif // _MYPOINT_