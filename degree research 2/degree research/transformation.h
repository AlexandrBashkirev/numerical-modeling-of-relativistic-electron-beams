#pragma once
#ifndef _TR_
#define _TR_
#include "math.h"

int ValueToPixel(float _value, float _rangeLeft,float _rangeRight, int _size)
{
	int tmp = abs((int)(  (_value - _rangeLeft)*_size/(fabs(_rangeLeft) + fabs(_rangeRight))  ));
	return tmp;
}
float PixelToValue(int _value, float _rangeLeft,float _rangeRight, int _size)
{
	float tmp = (float)( _value*(fabs(_rangeLeft) + fabs(_rangeRight))/_size + _rangeLeft  );
	return tmp;
}

#endif // _TR_