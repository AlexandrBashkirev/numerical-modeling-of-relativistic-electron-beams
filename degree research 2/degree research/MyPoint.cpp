#include "MyPoint.h"
#include "math.h"

s_MyPoint::~s_MyPoint()
{
}
s_MyPoint::s_MyPoint()
{
	m_x = 0.0f;
	m_y = 0.0f;
	m_color = QColor(255,0,0);
	m_id = 0;
}
s_MyPoint::s_MyPoint(float _x, float _y, QColor _color, int _id)
{
	m_x = _x;
	m_y = _y;
	m_color = _color;
	m_id = _id;
}
void s_MyPoint::DrawPoint(QWidget* _widget, int dx, int dy)
{
	QPainter painter(_widget);
	painter.setPen( m_color );
	
	int SizeCross = 1;

	painter.drawLine(  dx-SizeCross, dy-SizeCross, dx+SizeCross, dy+SizeCross );
	painter.drawLine( dx+SizeCross, dy-SizeCross, dx-SizeCross, dy+SizeCross );
}

float s_MyPoint::GetX()
{
	return m_x;
}
float s_MyPoint::GetY()
{
	return m_y;
}

s_MyPoint s_MyPoint::operator+(s_MyPoint &t_Vector)
{
	s_MyPoint tmp(m_x + t_Vector.m_x,m_y + t_Vector.m_y,m_color,m_id);

	return tmp;
}
s_MyPoint s_MyPoint::operator-(s_MyPoint &t_Vector)
{
	s_MyPoint tmp( m_x - t_Vector.m_x, m_y - t_Vector.m_y, m_color, m_id );

	return tmp;
}
s_MyPoint s_MyPoint::operator*(const float &t)
{
	s_MyPoint tmp( m_x, m_y, m_color, m_id );
	tmp.m_x *= t;
	tmp.m_y *= t;
	return tmp;
}
s_MyPoint s_MyPoint::operator/(const float &t)
{
	s_MyPoint tmp( m_x, m_y, m_color, m_id );
	tmp.m_x /= t;
	tmp.m_y /= t;
	return tmp;
}