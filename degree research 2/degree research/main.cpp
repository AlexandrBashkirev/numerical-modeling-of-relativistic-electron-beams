#include <QtGui/QApplication>
#include "degreeresearch.h"
#include <QTextCodec>


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	degreeresearch w;

	QTextCodec *codec = QTextCodec::codecForName("CP1251");
	QTextCodec::setCodecForCStrings(codec);

	setlocale(0,".1251");

	w.show();
	return a.exec();
}
