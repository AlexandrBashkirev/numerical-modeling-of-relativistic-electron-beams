#ifndef COUNTSTEPSFINISHCOMP_H
#define COUNTSTEPSFINISHCOMP_H

#include <QDialog>
#include "ui_countstepsfinishcomp.h"

class countStepsFinishComp : public QDialog
{
	Q_OBJECT

public:
	countStepsFinishComp(QWidget *parent = 0, unsigned int * t = 0);
	~countStepsFinishComp();

private:
	Ui::countStepsFinishCompClass ui;
	unsigned int* countSteps;

private slots:
	void on_ok_clicked();
	void on_close_clicked();
};

#endif // COUNTSTEPSFINISHCOMP_H
