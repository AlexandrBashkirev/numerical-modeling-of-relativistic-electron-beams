#ifndef _TIME_
#define _TIME_

#include "CoreUtilit.h"

class EXPORT c_Time
{
	clock_t m_timeWork;
	clock_t m_Elapsed;
	int m_ColTik;
public:
	c_Time(int _ColTik);
	~c_Time();
	float GetdT();
	bool UpdateScene();
	float GetTimeOneTik();
};

#endif // _TIME_