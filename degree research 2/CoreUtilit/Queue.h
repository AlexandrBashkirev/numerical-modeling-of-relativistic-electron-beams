#ifndef _QUEUE_
#define _QUEUE_

#include "CoreUtilit.h"

template <class T>
class  c_Queue
{
private:
	list<T> m_Queue;
	int m_colElem;
public:
	c_Queue(){m_colElem = 0;}
	void Push(T);
	T Pop();

	int GetColElem(){return m_colElem;};
};

template <class T>
void c_Queue<T>::Push(T _item)
{
	m_Queue.push_back( _item );
	m_colElem++;
}

template <class T>
T c_Queue<T>::Pop()
{
	T _temp = m_Queue.front();
	m_Queue.pop_front();
	m_colElem--;
	return _temp;
}

#endif // _QUEUE_