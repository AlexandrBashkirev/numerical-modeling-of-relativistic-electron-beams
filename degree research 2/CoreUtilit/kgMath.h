#ifndef _MATH_
#define _MATH_

#include "CoreUtilit.h"

#define PI 3.14159265358979323846f
#define EPSILON 0.00001f

struct EXPORT VECTOR3
{
	VECTOR3(float _x, float _y, float _z);
	VECTOR3();
	
	union
	{
		float v[3];
		struct
		{
			float x, y, z;
		};
	};
	

	VECTOR3 operator+(VECTOR3 &t_Vector);
	VECTOR3 operator-(VECTOR3 &t_Vector);
	VECTOR3 operator=(VECTOR3 &t_Vector);
	VECTOR3 operator=(const VECTOR3 &t_Vector);
	VECTOR3 operator+=(VECTOR3 &t_Vector);
	VECTOR3 operator-=(VECTOR3 &t_Vector);

	VECTOR3 operator*(float &t);
	VECTOR3 operator*(const float &t);
	VECTOR3 operator/(float &t);
	VECTOR3 operator/(const float &t);
	float operator*(VECTOR3 &t_Vector); // ��������� ���������
	VECTOR3 operator%(VECTOR3 &t_Vector); // ��������� ���������

	void norm();
	float length();
	VECTOR3 opposite();
	VECTOR3 proect(VECTOR3);
	void RotateX ( float angle );
	void RotateZ ( float angle );
	void RotateY ( float angle );
};

struct MATRIX4x4;

struct EXPORT VECTOR4
{
	VECTOR4(float _x, float _y, float _z, float _w);
	VECTOR4(VECTOR3 t_Vector3,float t_w);
	VECTOR4();
	
	union
	{
		float v[4];
		struct
		{
			float x, y, z, w;
		};
	};

	VECTOR4	operator+(VECTOR4 &t_Vector);
	VECTOR4	operator-(VECTOR4 &t_Vector);
	VECTOR4	operator=(VECTOR4 &t_Vector);
	VECTOR4	operator=(const VECTOR4 &t_Vector)
	{
		x = t_Vector.x;
		y = t_Vector.y;
		z = t_Vector.z;
		w = t_Vector.w;

		return *this;
	}
	VECTOR4	operator+=(VECTOR4 &t_Vector);
	VECTOR4	operator-=(VECTOR4 &t_Vector);
	VECTOR4	operator*(float &t);
	VECTOR4	operator*(const float &t);
	VECTOR4 operator/(float &t);
	VECTOR4 operator/(const float &t);
	float	operator*(VECTOR4 &t_Vector);
	VECTOR4 operator*(MATRIX4x4 &t);
	VECTOR4	operator%(VECTOR4 &t_Vector);
	_inline void norm();
	_inline float length();
	_inline VECTOR4 opposite();
	VECTOR4 proect(VECTOR4);  // ����������� ���������� ������ �� �����������, ���������� ������� ������ ���� ������ ������ �� ���������� �������
	
	/*_inline void RotateX ( float angle );
	_inline void RotateZ ( float angle );
	_inline void RotateY ( float angle );*/

};

struct EXPORT VECTOR4d
{
	VECTOR4d(double _x, double _y, double _z, double _w);
	VECTOR4d();
	
	union
	{
		double v[4];
		struct
		{
			double x, y, z, w;
		};
	};

	VECTOR4d	operator+(VECTOR4d &t_Vector);
	VECTOR4d	operator-(VECTOR4d &t_Vector);
	VECTOR4d	operator=(VECTOR4d &t_Vector);
	VECTOR4d	operator+=(VECTOR4d &t_Vector);
	VECTOR4d	operator-=(VECTOR4d &t_Vector);
	VECTOR4d	operator*(double &t);
	VECTOR4d	operator*(const double &t);
	VECTOR4d	operator/(double &t);
	VECTOR4d	operator/(const double &t);
	double	operator*(VECTOR4d &t_Vector);
	//VECTOR4d operator*(MATRIX4x4 &t);
	VECTOR4d	operator%(VECTOR4d &t_Vector);
	_inline void norm();
	_inline double length();
	_inline VECTOR4d opposite();
	VECTOR4d proect(VECTOR4d);  // ����������� ���������� ������ �� �����������, ���������� ������� ������ ���� ������ ������ �� ���������� �������
	
	/*_inline void RotateX ( float angle );
	_inline void RotateZ ( float angle );
	_inline void RotateY ( float angle );*/

};
struct EXPORT VECTOR4ld
{
	VECTOR4ld(long double _x, long double _y, long double _z, long double _w);
	VECTOR4ld();
	
	union
	{
		long double v[4];
		struct
		{
			long double x, y, z, w;
		};
	};

	VECTOR4ld	operator+(VECTOR4ld &t_Vector);
	VECTOR4ld	operator-(VECTOR4ld &t_Vector);
	VECTOR4ld	operator=(VECTOR4ld &t_Vector);
	VECTOR4ld	operator+=(VECTOR4ld &t_Vector);
	VECTOR4ld	operator-=(VECTOR4ld &t_Vector);
	VECTOR4ld	operator*(long double &t);
	VECTOR4ld	operator*(const long double &t);
	VECTOR4ld	operator/(long double &t);
	VECTOR4ld	operator/(const long double &t);
	long double	operator*(VECTOR4ld &t_Vector);
	//VECTOR4d operator*(MATRIX4x4 &t);
	VECTOR4ld	operator%(VECTOR4ld &t_Vector);
	_inline void norm();
	_inline long double length();
	_inline VECTOR4ld opposite();
	VECTOR4ld proect(VECTOR4ld);  // ����������� ���������� ������ �� �����������, ���������� ������� ������ ���� ������ ������ �� ���������� �������
	
	/*_inline void RotateX ( float angle );
	_inline void RotateZ ( float angle );
	_inline void RotateY ( float angle );*/

};

struct EXPORT MATRIX3x3
{
	union
	{
		float v[3][3];
		struct
		{
			float _11, _12, _13;
			float _21, _22, _23;
			float _31, _32, _33;
		};
	};

	MATRIX3x3();
	MATRIX3x3(float **);
	VECTOR3 operator*(VECTOR3 &t);
	MATRIX3x3 operator*(MATRIX3x3 &t);
	MATRIX3x3 operator=(MATRIX3x3 & t);

	bool inverse(MATRIX3x3 &);
	float det();
	
};

struct EXPORT MATRIX4x4
{
	float _11, _12, _13, _14;
	float _21, _22, _23, _24;
	float _31, _32, _33, _34;
	float _41, _42, _43, _44;
	
	MATRIX4x4();

	void E();
	VECTOR4 operator*(VECTOR4 &t);
	MATRIX4x4 operator*(MATRIX4x4 &t);
	float det();
	bool inverse(MATRIX4x4 &temp);

	MATRIX4x4	operator=(MATRIX4x4 &);
};

struct EXPORT Quaternion
{
	union
	{
		struct
		{
			float q0,q1,q2,q3;
		};
		struct
		{
			float q;
			VECTOR3 qv;
		};
	};

	Quaternion();
	Quaternion(float q, VECTOR3 qv);
	Quaternion(float q0, float q1, float q2, float q3);
};
struct EXPORT s_BBOX
{
	VECTOR3 min;
	VECTOR3 max;

	s_ColoredPoint* VBBOX;

	s_BBOX();
	s_BBOX(VECTOR3 _min, VECTOR3 _max);

	float GetDiagonal(){return (max-min).length();}
	VECTOR3 CalcCenter();
	bool PointInBox(VECTOR3 *pos);

	void CreateVBBOX();

	s_BBOX	operator=(s_BBOX &);
	
};

struct EXPORT s_Frustum
{
	VECTOR4 m_Frustum_Plane[5];
	void ColculFrustum( MATRIX4x4 _ViewMatrix, MATRIX4x4 _ProjMatrix );
	bool pointInFrustum (VECTOR3 v);
	float BoundingSphereInFrustum ( VECTOR4 BoundingSphere );
	float DistanseToFirstPlane( VECTOR4 BoundingSphere );
	bool BBoxInFrustum(s_BBOX *box);
	s_Frustum	operator=(s_Frustum &);
};


_inline EXPORT void MatrixRotateX ( MATRIX4x4 &RotX, float angle );
_inline EXPORT void MatrixRotateZ ( MATRIX4x4 &RotZ, float angle );
_inline EXPORT void MatrixRotateY ( MATRIX4x4 &RotY, float angle );
_inline EXPORT void GetMatrixTranslation(MATRIX4x4 &Translation, float x, float y, float z);
_inline EXPORT void GetMatrixTranslation(MATRIX4x4 &Translation, VECTOR3 Pos);

#endif // _MATH_