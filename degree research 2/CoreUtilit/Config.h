#ifndef _CONFIG_
#define _CONFIG_

#include "CoreUtilit.h"

enum e_TParam {FLOATPARAM, INTPARAM, STRINGPARAM, BOOLPARAM};
// ��������� ��������� ����������� ���� �� ���������� ������� �������������
// ���� ���������
struct cm_Param
{
	cm_Param()
	{
		FloatParam = 0.0f;
		IntParam = 0;
		StringParam = "";
		BoolParam = false;
	}
	///////// ��� ��������� /////////////
	e_TParam TypeParam;

	///////// �������� ��������� ////////
	float FloatParam;
	int IntParam;
	string StringParam;
	bool BoolParam;
};
struct s_StrParam
{
	string name;
	string value;
};
class EXPORT c_Config
{
protected:

	// ������ ���������� ����������� ��� �������� �������
	map<string, cm_Param> m_Config;
	// ����������� ������, ���������� ���, ��� ��������� � ��������� ��� � ������
	s_Error ParseLine( char* c_str );	

public:
	c_Config();
	~c_Config();

	bool Create(string NameConfig);
	bool Release();

	bool GetParam(string NameParam, float &value);
	bool GetParam(string NameParam, int &value);
	bool GetParam(string NameParam, string &value);
	bool GetParam(string NameParam, bool &value);

	vector<s_StrParam> GetAllStringParam();
};

class EXPORT c_MainConfig : public c_Config
{
private:
	c_MainConfig(void){this->Create("MainConfig.txt");}
	c_MainConfig(const c_MainConfig&){}
	~c_MainConfig(void){}

public:

	static c_MainConfig& instance()
	{
		static c_MainConfig obj;
		return obj;
	}
};

#define pMConfig c_MainConfig::instance()
#endif //_CONFIG_