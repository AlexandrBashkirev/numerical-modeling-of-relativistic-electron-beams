#ifndef _POINTS_
#define _POINTS_

#include "CoreUtilit.h"

struct s_Point
{
	float posX, posY, posZ;
};
struct s_ColoredPoint : public s_Point
{
	unsigned long col;

	s_ColoredPoint(){posX = 0.0; posY = 0.0; posZ = 0.0; col = 0x000000;}
	s_ColoredPoint(float _x, float _y, float _z, unsigned long _color) 
	{
		posX = _x; 
		posY = _y; 
		posZ = _z; 
		col = _color;
	}
};
struct s_PointWithTex : public s_Point
{
	float U, V;

	s_PointWithTex(){posX = 0.0; posY = 0.0; posZ = 0.0; U = 0.0; V = 0.0;}
	s_PointWithTex(float _x, float _y, float _z, float _u, float _v) 
	{
		posX = _x; 
		posY = _y; 
		posZ = _z; 
		U = _u;
		V = _v;
	}
};
struct s_PointWithTexNorm : public s_Point
{
	float normX, normY, normZ;
	float U, V;
};
struct s_PointWithTangent : public s_Point
{
	float normX, normY, normZ;
	float tanX, tanY, tanZ;
	float binormX, binormY, binormZ;
	float U, V;
};
struct s_SquareTerrain : public s_PointWithTexNorm
{
	float Tex1, Tex2, Tex3, Tex4;
	s_SquareTerrain(
					float _x, 
					float _y, 
					float _z, 
					float _u, 
					float _v, 
					float _normX, 
					float _normY, 
					float _normZ, 
					float _Tex1, 
					float _Tex2, 
					float _Tex3, 
					float _Tex4
				)
				{
					posX = _x; 
					posY = _y; 
					posZ = _z;  
					U = _u; 
					V = _v; 
					normX = _normX; 
					normY = _normY; 
					normZ = _normZ; 
					Tex1 = _Tex1; 
					Tex2 = _Tex2; 
					Tex3 = _Tex3; 
					Tex4 = _Tex4;
				}
	s_SquareTerrain()
				{
					posX = 0.0; 
					posY = 0.0; 
					posZ = 0.0;  
					U = 0.0; 
					V = 0.0; 
					normX = 0.0; 
					normY = 1.0; 
					normZ = 0.0; 
					Tex1 = 1.0; 
					Tex2 = 0.0; 
					Tex3 = 0.0; 
					Tex4 = 0.0;
				}
};

enum e_TypePoint
{
	Point,
	ColoredPoint,
	PointWithTexNorm,
	PointWithTangent,
	SquareTerrain,
	PointWithTex,
};
typedef s_Point* (*cmd)(int _size);
class c_PointFactory : public c_AbstractFactory<cmd>
{
	// ����������
protected:
	map<unsigned long, void*> m_DeclorationOfPoints;
public:
	bool SetDeclorationOfPoint(unsigned long _type, void* _decloration)
	{
		return m_DeclorationOfPoints.insert(pair<unsigned long, void*>(_type, _decloration)).second;
	}
	void* GetDeclorationOfPoint(unsigned long _type)
	{
		map<unsigned long, void*>::iterator _iDeclorationOfPoints;
		_iDeclorationOfPoints =  m_DeclorationOfPoints.find(_type);
		if(_iDeclorationOfPoints ==  m_DeclorationOfPoints.end())
		{
			pLog->SetError("c_PointsMenager: ������� �������� ���������� �� ������������������� ����",3,0);
			return 0;
		}
		return _iDeclorationOfPoints->second;
	}
		// ���������� ��� �����������
protected:
	map<unsigned long, void*> m_DeclorationOfPointsForInst;
public:
	bool SetDeclorationOfPointForInst(unsigned long _type, void* _decloration)
	{
		return m_DeclorationOfPointsForInst.insert(pair<unsigned long, void*>(_type, _decloration)).second;
	}
	void* GetDeclorationOfPointForInst(unsigned long _type)
	{
		map<unsigned long, void*>::iterator _iDeclorationOfPoints;
		_iDeclorationOfPoints =  m_DeclorationOfPointsForInst.find(_type);
		if(_iDeclorationOfPoints ==  m_DeclorationOfPointsForInst.end())
		{
			pLog->SetError("c_PointsMenager: ������� �������� ���������� ��� ����������� �� ������������������� ����",3,0);
			return 0;
		}
		return _iDeclorationOfPoints->second;
	}
	// FVF
protected:
	map<unsigned long, unsigned long> m_FVFOfPoints;
public:
	bool SetFVFOfPoint(unsigned long _type, unsigned long _fvf)
	{
		return m_FVFOfPoints.insert(pair<unsigned long, unsigned long>(_type, _fvf)).second;
	}
	unsigned long GetFVFOfPoint(unsigned long _type)
	{
		map<unsigned long, unsigned long>::iterator _iFVFOfPoints;
		_iFVFOfPoints =  m_FVFOfPoints.find(_type);
		if(_iFVFOfPoints ==  m_FVFOfPoints.end())
		{
			pLog->SetError("c_PointsMenager: ������� �������� fvf �� ������������������� ����",3,0);
			return 0;
		}
		return _iFVFOfPoints->second;
	}
	// ������
protected:
	map<unsigned long, unsigned long> m_SizeOfPoints;
public:
	bool SetSizeOfPoint(unsigned long _type, unsigned long _size)
	{
		return m_SizeOfPoints.insert(pair<unsigned long, unsigned long>(_type, _size)).second;
	}
	unsigned long GetSizeOfPoint(unsigned long _type)
	{
		map<unsigned long, unsigned long>::iterator _iSizeOfPoints;
		_iSizeOfPoints =  m_SizeOfPoints.find(_type);
		if(_iSizeOfPoints ==  m_SizeOfPoints.end())
		{
			pLog->SetError("c_PointsMenager: ������� �������� ������ �� ������������������� ����",3,0);
			return 0;
		}
		return _iSizeOfPoints->second;
	}


	static c_PointFactory& instance()
	{
		static c_PointFactory obj;
		return obj;
	}
};
#define pPointFactory c_PointFactory::instance()
namespace 
{
	s_Point* CreatePoint(int _size)
	{
		return new s_Point [_size];
	};
	const bool insertPoint = pPointFactory.RegisterCallBacks(Point, CreatePoint);
	const bool insertSizePoint = pPointFactory.SetSizeOfPoint(Point, sizeof(s_Point));
}
namespace 
{
	s_Point* CreatePointWithTex(int _size)
	{
		return new s_PointWithTex [_size];
	};
	const bool insertPointWithTex = pPointFactory.RegisterCallBacks(PointWithTex, CreatePointWithTex);
	const bool insertSizePointWithTex = pPointFactory.SetSizeOfPoint(PointWithTex, sizeof(s_PointWithTex));
}
namespace 
{
	s_Point* CreateColoredPoint(int _size)
	{
		return new s_ColoredPoint [_size];
	};
	const bool insertColoredPoint = pPointFactory.RegisterCallBacks(ColoredPoint, CreateColoredPoint);
	const bool insertSizeColoredPoint = pPointFactory.SetSizeOfPoint(ColoredPoint, sizeof(s_ColoredPoint));
}
namespace 
{
	s_Point* CreatePointWithTexNorm(int _size)
	{
		return new s_PointWithTexNorm [_size];
	};
	const bool insertPointWithTexNorm = pPointFactory.RegisterCallBacks(PointWithTexNorm, CreatePointWithTexNorm);
	const bool insertSizePointWithTexNorm = pPointFactory.SetSizeOfPoint(PointWithTexNorm, sizeof(s_PointWithTexNorm));
}
namespace 
{
	s_Point* CreatePointWithTangent(int _size)
	{
		return new s_PointWithTangent [_size];
	};
	const bool insertPointWithTangent = pPointFactory.RegisterCallBacks(PointWithTangent, CreatePointWithTangent);
	const bool insertSizePointWithTangent = pPointFactory.SetSizeOfPoint(PointWithTangent, sizeof(s_PointWithTangent));
}
namespace 
{
	s_Point* CreateSquareTerrain(int _size)
	{
		return new s_SquareTerrain [_size];
	};
	const bool insertSquareTerrain = pPointFactory.RegisterCallBacks(SquareTerrain, CreateSquareTerrain);
	const bool insertSizeSquareTerrain = pPointFactory.SetSizeOfPoint(SquareTerrain, sizeof(s_SquareTerrain));
}

#endif // _POINTS_