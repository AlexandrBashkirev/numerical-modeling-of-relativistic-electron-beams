#ifndef _EVENT_
#define _EVENT_
#include "CoreUtilit.h"

class EXPORT c_PropertyOfEvent
{
};

class EXPORT c_Event
{
public:
	c_Event()
	{
		m_value = false;
		m_property = 0;
	}
	c_Event(bool _value)
	{
		m_value = _value;
	}
	~c_Event()
	{
		if(m_property)
			delete m_property;
	}

	virtual bool EventHappened()
	{
		return m_value;
	}
	c_PropertyOfEvent* GetParamEvent(){return m_property;}
	virtual bool ItNotNullEvent(){return true;}

	virtual bool operator==(c_Event _e)
	{
		return EventHappened() == _e.EventHappened();
	};

	virtual bool operator==(bool _value)
	{
		return _value == EventHappened();
	};
	
protected:
	c_PropertyOfEvent* m_property;

private:
	bool m_value;
};

class EXPORT c_EmptyEvent : public c_Event
{
public:
	bool ItNotNullEvent(){return false;}
};

#endif // _EVENT_