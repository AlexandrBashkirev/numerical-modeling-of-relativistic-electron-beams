#ifndef _ASSOCIATION_
#define _ASSOCIATION_
#include "CoreUtilit.h"

class c_Association
{
public:
	c_Association(c_Event* _event ,void (*cmp)(c_PropertyOfEvent*))
	{
		m_countEvents = 1;
		m_reaction1 = cmp;
		m_events[0] = _event;
		m_events[1] = 0;
		m_events[2] = 0;
		m_events[3] = 0;
	}
	c_Association(c_Event* _event1, c_Event* _event2,void (*cmp)(c_PropertyOfEvent*, c_PropertyOfEvent*))
	{
		m_countEvents = 2;
		m_reaction2 = cmp;
		m_events[0] = _event1;
		m_events[1] = _event2;
		m_events[2] = 0;
		m_events[3] = 0;
	}
	c_Association(c_Event* _event1, c_Event* _event2, c_Event* _event3, void (*cmp)(c_PropertyOfEvent*, c_PropertyOfEvent*, c_PropertyOfEvent*))
	{
		m_countEvents = 3;
		m_reaction3 = cmp;
		m_events[0] = _event1;
		m_events[1] = _event2;
		m_events[2] = _event3;
		m_events[3] = 0;
	}
	c_Association(c_Event* _event1, c_Event* _event2, c_Event* _event3, c_Event* _event4 ,void (*cmp)(c_PropertyOfEvent*, c_PropertyOfEvent*, c_PropertyOfEvent*, c_PropertyOfEvent* ))
	{
		m_countEvents = 4;
		m_reaction4 = cmp;
		m_events[0] = _event1;
		m_events[1] = _event2;
		m_events[2] = _event3;
		m_events[3] = _event4;
	}
	~c_Association(){}

	void ProcessingAssociation()
	{
		bool EventsHappened = true;
		for(int i = 0; i < m_countEvents; i++)
		{
			if(EventsHappened)
				EventsHappened = m_events[i]->EventHappened();
		}
		if(EventsHappened)
			switch (m_countEvents)
			{
			case 1:
				(*m_reaction1)(m_events[0]->GetParamEvent());
				break;
			case 2:
				(*m_reaction2)(m_events[0]->GetParamEvent(), m_events[1]->GetParamEvent());
				break;
			case 3:
				(*m_reaction3)(m_events[0]->GetParamEvent(), m_events[1]->GetParamEvent(), m_events[2]->GetParamEvent());
				break;
			case 4:
				(*m_reaction4)(m_events[0]->GetParamEvent(), m_events[1]->GetParamEvent(), m_events[2]->GetParamEvent(), m_events[3]->GetParamEvent());
				break;
			}
			
	}

	c_Event* m_events[4];
	int m_countEvents;

	void (*m_reaction1)(c_PropertyOfEvent*);
	void (*m_reaction2)(c_PropertyOfEvent*, c_PropertyOfEvent*);
	void (*m_reaction3)(c_PropertyOfEvent*, c_PropertyOfEvent*, c_PropertyOfEvent*);
	void (*m_reaction4)(c_PropertyOfEvent*, c_PropertyOfEvent*, c_PropertyOfEvent*, c_PropertyOfEvent* );
};

#endif // _ASSOCIATION_