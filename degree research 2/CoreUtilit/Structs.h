#ifndef _STRUCTS_
#define _STRUCTS_

#include "CoreUtilit.h"

enum EditorMade{EMNull, EMRotate, EMScale, EMTranslate};

struct s_Color
{
	char m_r, m_g, m_b, m_a;

	s_Color(char _r,char _g, char _b)
	{
		m_r = _r;
		m_g = _g;
		m_b = _b;
		m_a = (char)255;
	};
	s_Color(char _r,char _g, char _b, char _a)
	{
		m_r = _r;
		m_g = _g;
		m_b = _b;
		m_a = _a;
	};
};
// ��������� �������. ��� �������� ������� �� ������� 3dkg
struct s_KGXInfoOfSubset
{
	DWORD SubsetNumber;//����� �������/����������
	DWORD FaceStart;// ����� ��������� ����� 
	DWORD FaceCount;// ���-�� ����� � ���� ���������� 
	DWORD VertexStart;// ����� ��������� ������� 
	DWORD VertexCount;// ���������� ������ � ���������� 
};
///////////////////////////////////// ��������� ������ ���������
/*
struct s_Square_Terrain_Tan 
{
s_Square_Terrain_Tan(
	float x, 
	float y, 
	float z, 
	float u, 
	float v, 
	float a, 
	float b, 
	float c, 
	float Tex1, 
	float Tex2, 
	float Tex3, 
	float Tex4,
	float Tan_x,
	float Tan_y,
	float Tan_z
	)
	{
		_x = x; 
		_y = y; 
		_z = z; 
		_u = u; 
		_v = v; 
		_a = a; 
		_b = b; 
		_c = c; 
		_Tex1 = Tex1; 
		_Tex2 = Tex2; 
		_Tex3 = Tex3; 
		_Tex4 = Tex4;
		_Tan_x = Tan_x;
		_Tan_y = Tan_y;
		_Tan_z = Tan_z;
		_Tan_w = 0.0;
	}
 float _x, _y, _z;
 float _a, _b, _c;
 float _u, _v;
 float _Tex1,_Tex2,_Tex3,_Tex4;
 float _Tan_x, _Tan_y, _Tan_z, _Tan_w;


 static const DWORD FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX5;
};
struct s_Square_Terrain 
{
s_Square_Terrain(
	float x, 
	float y, 
	float z, 
	float u, 
	float v, 
	float a, 
	float b, 
	float c, 
	float Tex1, 
	float Tex2, 
	float Tex3, 
	float Tex4
	)
	{
		_x = x; 
		_y = y; 
		_z = z; 
		_u = u; 
		_v = v; 
		_a = a; 
		_b = b; 
		_c = c; 
		_Tex1 = Tex1; 
		_Tex2 = Tex2; 
		_Tex3 = Tex3; 
		_Tex4 = Tex4;
	}
 float _x, _y, _z;
 float _a, _b, _c;
 float _u, _v;
 float _Tex1,_Tex2,_Tex3,_Tex4;


 static const DWORD FVF = D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX3;
};
*/
///////////////////////////////// ��������������� ��� �������� ����������
struct s_TerrainSettings
{
	int m_SizeMap;
	int m_SizeMeshMap[4];
	int m_Cet[4];
};
struct s_NewMapSettings
{
	string m_NameMap;
	int m_SizeSector;
	int m_GlobSizeMap;
	int m_ColObjectInSector;
	int m_SizeMeshMap;
};
struct s_RenderInfo
{
	s_RenderInfo()
	{
		countPolys = 0;
		countDIPs = 0;
		countObjectInFirstLOD = 0;
		countObjectInSecondLOD = 0;
		countObjectInThirdLOD = 0;
		fps = 0;
	}
	void operator=(s_RenderInfo t)
	{
		this->countPolys = t.countPolys;
		this->countDIPs = t.countDIPs;
		this->countObjectInFirstLOD = t.countObjectInFirstLOD;
		this->countObjectInSecondLOD = t.countObjectInSecondLOD;
		this->countObjectInThirdLOD = t.countObjectInThirdLOD;
		this->fps = t.fps;
	}
	unsigned long int countPolys;
	unsigned int countDIPs;
	unsigned int countObjectInFirstLOD;
	unsigned int countObjectInSecondLOD;
	unsigned int countObjectInThirdLOD;
	unsigned int fps;
};
struct s_Camera_Settings
{
	int m_Global_PosY;
	int m_Global_PosX;
};
#endif // _STRUCTS_