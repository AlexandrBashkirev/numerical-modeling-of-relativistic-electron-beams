#ifndef _TYPELIST_
#define _TYPELIST_
#include "CoreUtilit.h"

class s_NullType{};
template <class T, class U>
struct s_TypeList
{
	typedef T head;
	typedef U tail;
};

/*template <class ClassT> struct s_Length;
template <class T = s_NullType> struct s_Length
{
	enum {value = 0};
};
template <class T, class U>
struct s_Length
{
	enum {value = 1 + s_Length<U>::value };
};*/

#define TYPELIST1(T1) s_TypeList<T1,s_NullType>
#define TYPELIST2(T1, T2) s_TypeList<T1, s_TypeList<T2, s_NullType> >
#define TYPELIST3(T1, T2, T3) s_TypeList<T1, s_TypeList<T2, s_TypeList<T3, s_NullType> > > 
#define TYPELIST4(T1, T2, T3, T4) s_TypeList<T1,s_TypeList<T2, s_TypeList<T3, s_TypeList<T4, s_NullType> > > >

#endif