#ifndef _ABSTRACTFACTORY_
#define _ABSTRACTFACTORY_
#include "CoreUtilit.h"

template <class T>
class c_AbstractFactory
{
protected:
	map<unsigned long, T> m_Callbacks;
public:
	bool RegisterCallBacks(unsigned long _type, T _callback){return m_Callbacks.insert(pair<unsigned long, T>(_type, _callback)).second;}
	T GetCallBacks(unsigned long _type)
	{
		map<unsigned long, T>::iterator _iCalBacks;
		_iCalBacks =  m_Callbacks.find(_type);
		if(_iCalBacks ==  m_Callbacks.end())
		{
			pLog->SetError("AbstractFactory: ������� �������� ��������� �� ������� �� ������������� ����",3,0);
			return 0;
		}
		return _iCalBacks->second;
	}
};

#endif //_ABSTRACTFACTORY_