#ifndef _STACK_
#define _STACK_

#include "CoreUtilit.h"

template <class T>
class c_Stack
{
private:
	list<T> m_Stack;
	int m_colElem;
public:
	c_Stack(){m_colElem = 0;}
	void Push(T);
	T Pop();

	int GetColElem(){return m_colElem;};
};

template <class T>
void c_Stack<T>::Push(T _item)
{
	m_Stack.push_back( _item );
	m_colElem++;
}

template <class T>
T c_Stack<T>::Pop()
{
	T _temp = m_Stack.back();
	m_Stack.pop_back();
	m_colElem--;
	return _temp;
}

#endif // _STACK_