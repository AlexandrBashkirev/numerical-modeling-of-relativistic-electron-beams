#include <string>
#include <vector>
#include <list>
#include <map>
#include <fstream>
#include <iostream>
#include <math.h>
#include <Time.h>
#include <TChar.h>
#include <windows.h>

using namespace std;

#define BUILD_DLL


#if defined(BUILD_DLL)
#  define EXPORT __declspec(dllexport)
#else
# if defined(BUILD_APP)
#  define EXPORT __declspec(dllimport)
# else
#  define EXPORT
# endif
#endif


#ifndef kgNAMELOG
	#define kgNAMELOG "Log_KerGame"
#endif


#ifndef _COREUTILIT_
#define _COREUTILIT_

extern "C++"
{
#include "Log.h"
#include "Config.h"
#include "AbstractFactory.h"
#include "Points.h"
#include "kgMath.h"
#include "Camera.h"
#include "Structs.h"
#include "Time.h"
#include "Stack.h"
#include "Queue.h"
#include "TypeList.h"
#include "c_Event.h"
#include "c_Association.h"

};

#endif // _COREUTILIT_
