#ifndef _LOG_
#define _LOG_

#include "CoreUtilit.h"

#define CREATE_MSG	s_Error TempMSG("",0,0);
#define SEND_MSG	return TempMSG;

#define LOST_DEVICE 1


struct EXPORT s_Error
{
public:
	s_Error(string _TextError, int _TypeError,int _IDError)
	{
		TextError = _TextError;
		TypeError = _TypeError;
		IDError = _IDError;
	}
	s_Error()
	{
		TextError = "";
		TypeError = 0;
		IDError = 0;
	}
	string TextError;
	int TypeError;
	int IDError;
};
struct s_Log_Settings
{
	string m_Name_File_Log;
	string m_Title;
	string m_Name_Logo;
	string m_Name_Prog;
	string m_Year;
	string m_Name_Compani;
	string m_Version;
};
class EXPORT c_ErrorHunter
{
	int LastError;
	// ���� �������
	fstream* m_File_Log;

	c_ErrorHunter(void);
	c_ErrorHunter(const c_ErrorHunter&);
	~c_ErrorHunter(void);

	struct s_SharedInfoLog
	{
		c_ErrorHunter* mp_Log;
		long m_countUsers;
	};

	s_SharedInfoLog* m_SharedInfo;
	static c_ErrorHunter* m_Log;
	HANDLE m_hfm;

public:

	static c_ErrorHunter* instance();

	void SetError(s_Error p_Error);
	void SetError(string _TextError, int _TypeError, int _IDError);
	void SetError(string _TextError, int _TypeError);
	void SetError(int _TypeError,int _IDError,char* _TextError, ... );
	void SetError(int _TypeError, char* _TextError, ... );
	void SetMessage( char*, ... );
	void SetMessage( string );

	void StopProgramm();
	int  EHGetLastError();
};

#define pLog c_ErrorHunter::instance()

#endif // _LOG_