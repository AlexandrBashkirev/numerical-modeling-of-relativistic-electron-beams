#ifndef _CAMERA_
#define _CAMERA_

#include "CoreUtilit.h"

class EXPORT c_Camera
{
	//////// ��������
	VECTOR4 m_Pos_Plauer;
	float	m_Distance;
	float	m_PitchAngle, m_YawAngle;
	float	m_Speed;
	bool	m_ChangeParam;
	enum CameraMode { FIRST_PERSON, THIRD_PERSON } m_Mode;

	///////// ���������
	VECTOR4 m_Global_Pos_Cam; 
	VECTOR4 m_up;		// ��������� ���������
	VECTOR4 m_look;		//
	
	//////// ���������� ( ��� �������� �������� )
	VECTOR4 m_Temp_Pos_Plauer;
	float	m_Temp_Distance;
	float	m_Temp_PitchAngle, m_Temp_YawAngle;
	CameraMode m_Temp_mode;

	/////// ��������� ��������� ��������� ��������� (Frustum)
	s_Frustum m_Frustum;

	/////// ������� ����
	MATRIX4x4 m_ViewMatrix;

	// �������� � ����������� ������� � � ��� ��������������
	float m_Away;
	float m_Side;
	
public:

	c_Camera();
	~c_Camera();

	// �������������
		// ������������� �������� �������� ������ �� ������� � �������
	void SetSpeed(float );
		// ������������� ����� ������ ������
	void SetFirstPersonMode();
	void SetThirdPersonMode();
		//  ������������� ��������������� ���������, ������������ ������� �������
	void SetPositionSlowly(VECTOR4 );
	void SetRotateSlowly(float , float);
	void SetDistanceSlowly(float);

	void SetPosition(VECTOR4 );
	void SetRotate(float , float);
	void SetDistance(float);

	void SetGlobSizeMap(int);
	
	// ������ - ����������� � ���������� ������� ���� ������ 

	void Update();

	MATRIX4x4 GetViewMatrix();
	VECTOR4 GetPosPlayer();
	VECTOR4 GetPosCam();
	float GetDistance();
	
	// ����������� - ���������� � �������������� ���������� ���� ���������
	// �� ������������� �� ���������� ������� ��������� ��������� ������

	void AddPosition  (VECTOR4 ); 
	void AddRotate(float , float); 
	void AddDistance(float);
	void Away(float _away){m_Away += _away;} // ��� �����/�����
	void SideAway(float _side){m_Side += _side;} // ������ / �����


	// ������ ������� � ���������� ��������� �� ����
	s_Frustum GetFrustum(MATRIX4x4);
};

#endif
