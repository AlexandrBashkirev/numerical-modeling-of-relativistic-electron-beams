#include "stdafx.h"
#include "c_RK2_SC_GPU_VA_Lag_Diod.h"


c_RK2_SC_GPU_VA_Lag_Diod::c_RK2_SC_GPU_VA_Lag_Diod() : c_GPUInit() 
{
	char* t[] = {"KernelSC","KernelRK2", "KernelEBInit"};
	InitGPU("RK2_SC_GPU_VA_Lag.cl", 3, t);
	m_oldCompTime = 0;
}
c_RK2_SC_GPU_VA_Lag_Diod::~c_RK2_SC_GPU_VA_Lag_Diod()
{
	clReleaseMemObject(clmemFC);
	clReleaseMemObject(clmemFV);
	clReleaseMemObject(clmemSA);
	clReleaseMemObject(clmemSV);
	clReleaseMemObject(clmemSC);
	clReleaseMemObject(clmemE);
	clReleaseMemObject(clmemB);
	clReleaseMemObject(clmemCountParticl);
}
void c_RK2_SC_GPU_VA_Lag_Diod::InitOCLBUF()
{
	int err = 0;

	cl_image_format imageFormat;
	imageFormat.image_channel_data_type = CL_FLOAT;
	imageFormat.image_channel_order = CL_RGBA;

	clmemTexA = clCreateImage2D( GPUContext, CL_MEM_READ_ONLY, &imageFormat, TexSize, TexSize, 0, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemTexA", 2);
	clmemTexV = clCreateImage2D( GPUContext, CL_MEM_READ_ONLY, &imageFormat, TexSize, TexSize, 0, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemTexV", 2);
	clmemTexC = clCreateImage2D( GPUContext, CL_MEM_READ_ONLY, &imageFormat, TexSize, TexSize, 0, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemTexC", 2);

	clmemCountParticl = clCreateBuffer(GPUContext, CL_MEM_READ_ONLY, sizeof(cl_int)*TexSize, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemCountParticl", 2);

	clmemFC = clCreateBuffer(GPUContext, CL_MEM_READ_ONLY, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemFC", 2);
	clmemFV = clCreateBuffer(GPUContext, CL_MEM_READ_ONLY, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemFV", 2);

	clmemSA = clCreateBuffer(GPUContext, CL_MEM_WRITE_ONLY, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemSA", 2);
	clmemSV = clCreateBuffer(GPUContext, CL_MEM_WRITE_ONLY, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemSV", 2);
	clmemSC = clCreateBuffer(GPUContext, CL_MEM_WRITE_ONLY, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemSC", 2);

	clmemE = clCreateBuffer(GPUContext, CL_MEM_READ_WRITE, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemE", 2);
	clmemB = clCreateBuffer(GPUContext, CL_MEM_READ_WRITE, sizeof(cl_float4)*m_maxCountParticle, NULL, &err);
	if(err!=CL_SUCCESS) pLog->SetError("clmemB", 2);

	if(err!=CL_SUCCESS)
	{
		switch (err)
		{
		case CL_INVALID_CONTEXT:
			pLog->SetError("CL_INVALID_CONTEXT", 3);
			break;
		case CL_INVALID_VALUE:
			pLog->SetError("CL_INVALID_VALUE", 3);
			break;
		case CL_INVALID_BUFFER_SIZE:
			pLog->SetError("CL_INVALID_BUFFER_SIZE Error", 3);
			break;
		case CL_INVALID_HOST_PTR:
			pLog->SetError("CL_INVALID_HOST_PTR", 3);
			break;
		case  CL_MEM_OBJECT_ALLOCATION_FAILURE:
			pLog->SetError("CL_MEM_OBJECT_ALLOCATION_FAILURE", 3);
			break;
		case CL_INVALID_OPERATION:
			pLog->SetError("CL_INVALID_OPERATION Error", 3);
			break;
		case  CL_OUT_OF_HOST_MEMORY:
			pLog->SetError("CL_OUT_OF_HOST_MEMORY Error", 3);
			break;
		};
		pLog->SetError("Init-Memory Error", 3);
		exit(1);
	}
}
void c_RK2_SC_GPU_VA_Lag_Diod::Create(s_paramColculate& _paramColculate)
{
	m_paramColculate = _paramColculate;
	float tmp = fabs(S()*m_paramColculate.directionStartV.z*m_paramColculate.V*m_paramColculate.T*m_paramColculate.Density/(e*m_paramColculate.k));
	m_countParticleAddedAtStap = (int)(tmp);
	m_curentCountParticls = 0;
	m_maxCountParticle = m_paramColculate.countSteps*m_countParticleAddedAtStap;

	m_RangeX = sizeCathodeX()*3.0f;
	m_RangeY = sizeCathodeY()*3.0f;
	m_RangeZ = m_paramColculate.directionStartV.z*m_paramColculate.V*m_paramColculate.T*m_paramColculate.countSteps*1.1f;

	InitOCLBUF();

	// ������� ������ ��������
	m_curentCountParticls += m_countParticleAddedAtStap;

	long countPart = 0;

	AddNewPart(countPart,0);
}
void c_RK2_SC_GPU_VA_Lag_Diod::Create(s_fullParamColculate& _paramColculate)
{
	m_paramColculate = FullParamToParam(_paramColculate);
	float tmp = fabs(S()*m_paramColculate.directionStartV.z*m_paramColculate.V*m_paramColculate.T*m_paramColculate.Density/(e*m_paramColculate.k));
	m_countParticleAddedAtStap = (int)(tmp);
	m_curentCountParticls = m_countParticleAddedAtStap*_paramColculate.countComputedSteps;
	m_startStep = _paramColculate.countComputedSteps + 1;
	m_curentCountSteps = _paramColculate.countComputedSteps;// - 1;
	//m_startStep = _paramColculate.countComputedSteps;
	m_oldCompTime = _paramColculate.computeTime*CLOCKS_PER_SEC;

	m_RangeX = sizeCathodeX()*3.0f;
	m_RangeY = sizeCathodeY()*3.0f;
	m_RangeZ = m_paramColculate.directionStartV.z*m_paramColculate.V*m_paramColculate.T*m_paramColculate.countSteps*1.1f;

	InitOCLBUF();
}
void c_RK2_SC_GPU_VA_Lag_Diod::StopCompute()
{
}
void c_RK2_SC_GPU_VA_Lag_Diod::ComputeSC(int countPartI, int offset)
{
	int err = 0;

	float Mult = 4.0f * PI * 8.8541878f / (-1.602e-7f * m_paramColculate.k);
	
	size_t origin[3];
	origin[0] = 0;
	origin[1] = 0;
	origin[2] = 0;
	size_t region[3];
	region[0] = TexSize;//maxCountParticl; // ������ ������
	region[1] = countPartI;  // ���������� �����
	region[2] = 1;

	cl_event  eventsTexCopy[4];

	err += clEnqueueWriteImage( GPUCommandQueue, clmemTexA, CL_TRUE, origin, region, 0/*sizeof(cl_float4)*countPartI*/, 0, dataTexA, 0, NULL, &eventsTexCopy[0]);
	err += clEnqueueWriteImage(	GPUCommandQueue, clmemTexV, CL_TRUE, origin, region, 0/*sizeof(cl_float4)*countPartI*/, 0, dataTexV, 0, NULL, &eventsTexCopy[1]);
	err += clEnqueueWriteImage(	GPUCommandQueue, clmemTexC, CL_TRUE, origin, region, 0/*sizeof(cl_float4)*countPartI*/, 0, dataTexC, 0, NULL, &eventsTexCopy[2]);
	err += clEnqueueWriteBuffer ( GPUCommandQueue, clmemCountParticl, CL_TRUE, 0, sizeof(cl_int)*countPartI, countParticl, 0, NULL, &eventsTexCopy[3]);

	if(err != CL_SUCCESS){pLog->SetError("Write-Arguments Error", 2);return;}

	clWaitForEvents(4, eventsTexCopy);
	err += clReleaseEvent(eventsTexCopy[0]);
	err += clReleaseEvent(eventsTexCopy[1]);
	err += clReleaseEvent(eventsTexCopy[2]);
	err += clReleaseEvent(eventsTexCopy[3]);

	err += clSetKernelArg( clKernel[0], 0, sizeof(cl_mem),		(void*)&clmemTexA );
	err += clSetKernelArg( clKernel[0], 1, sizeof(cl_mem),		(void*)&clmemTexV );
	err += clSetKernelArg( clKernel[0], 2, sizeof(cl_mem),		(void*)&clmemTexC );
	err += clSetKernelArg( clKernel[0], 3, sizeof(cl_mem),		(void*)&clmemCountParticl );
	err += clSetKernelArg( clKernel[0], 4, sizeof(cl_mem),		(void*)&clmemE );
	err += clSetKernelArg( clKernel[0], 5, sizeof(cl_mem),		(void*)&clmemB );
	err += clSetKernelArg( clKernel[0], 6, sizeof(cl_int),		(void*)&offset );
	err += clSetKernelArg( clKernel[0], 7, sizeof(cl_float),	(void*)&Mult );

	if(err!=CL_SUCCESS){pLog->SetError("Setting-Arguments Error", 2);return;}

	//***********************EXECUTING KERNEL**********************
	size_t WorkSize[1] = {countPartI};
	err += clEnqueueNDRangeKernel(GPUCommandQueue, clKernel[0], 1, NULL, WorkSize, NULL, 0, NULL, NULL);
	err += clFlush(GPUCommandQueue);

	if(err!=CL_SUCCESS){pLog->SetError("Executing Error", 2);return;}

	for(int i = 0; i < countPartI; i++ )
		countParticl[i] = 0;
}
void c_RK2_SC_GPU_VA_Lag_Diod::checkCountPart(int cs)
{
#ifdef NDEBUG
	#pragma omp parallel for num_threads(3)
#endif
	for(int i = 0; i < m_curentCountParticls; i++)
	{
		if( m_Coordinate[cs-1][i].z >= m_paramColculate.sizeZ )
			m_Activity[cs-1][i] = false;
		else
			m_Activity[cs-1][i] = true;
	}
}
void c_RK2_SC_GPU_VA_Lag_Diod::StartCompute()
{
	for(int i = 0; i < TexSize; i++ )
		countParticl[i] = 0;

	m_startCompTime = GetTickCount();
	size_t WorkSize[1];
	int err = 0;

	for(int cs = m_startStep ; cs < m_paramColculate.countSteps; cs++ )
	{
		Sleep(10);
		checkCountPart( cs );

		//////////////////////////////////////////
		// ��������� ������� ������ ����
		unsigned int countPart = m_curentCountParticls;
		m_curentCountParticls += m_countParticleAddedAtStap;
		AddNewPart(countPart, cs);

		/*bool* _a = new bool[m_curentCountParticls];
		m_Activity.push_back(_a);
#pragma omp parallel for num_threads(3)
		for(int i = 0; i < m_curentCountParticls; i++)
			m_Activity[cs][i] = true;*/

		m_Shift.insert(m_Shift.end(), m_curentCountParticls);

		//////////////////////////////////////////
		// �������������� ����
		err = 0;
		err += clSetKernelArg(clKernel[2], 0, sizeof(cl_mem),		(void*)&clmemE);
		err += clSetKernelArg(clKernel[2], 1, sizeof(cl_mem),		(void*)&clmemB);
		err += clSetKernelArg(clKernel[2], 2, sizeof(cl_float4),	(void*)&VECTOR4(	m_paramColculate.directionE.x*m_paramColculate.E, 
																						m_paramColculate.directionE.y*m_paramColculate.E, 
																						m_paramColculate.directionE.z*m_paramColculate.E, 
																						0.0));
		err += clSetKernelArg(clKernel[2], 3, sizeof(cl_float4),	(void*)&VECTOR4(	m_paramColculate.directionB.x*m_paramColculate.B, 
																						m_paramColculate.directionB.y*m_paramColculate.B, 
																						m_paramColculate.directionB.z*m_paramColculate.B, 
																						0.0));

		//***********************EXECUTING KERNEL**********************
		size_t WorkSize[1] = {countPart};
		err += clEnqueueNDRangeKernel(GPUCommandQueue, clKernel[2], 1, NULL, WorkSize, NULL, 0, NULL, NULL);
		err += clFlush(GPUCommandQueue);
	
		if(err!=CL_SUCCESS){pLog->SetError("Executing Error", 2);return;}

///////////////////////////////
		// ���� ���� ����������������� ������
		unsigned int t = cs - 1;
		for(int a = 0; a < countPart/TexSize + 1; a++)
		{
			int I = TexSize;
			if(a == countPart/TexSize)
				I = countPart - a*TexSize;
			for(int b = 0; b < countPart/TexSize + 1; b++)
			{
				int J = TexSize;
				if(b == countPart/TexSize)
					J = countPart - b*TexSize;

#pragma omp parallel for num_threads(3)
				for(int i = 0; i < I; i++)
				{
					for(int j = 0; j < J; j++)
					{
						if( m_Activity[t][i+a*TexSize] && i+a*TexSize != j+b*TexSize)
						{
							int tau = lagResolve( i+a*TexSize, j+b*TexSize, t );
					
							if(tau >= 0 && m_Activity[tau][j+b*TexSize])
							{
								dataTexA[i][countParticl[i]] = m_Acceleration[tau][j+b*TexSize];
								dataTexV[i][countParticl[i]] = m_Velocity[tau][j+b*TexSize];
								dataTexC[i][countParticl[i]] = m_Coordinate[tau][j+b*TexSize];

								dataTexV[i][countParticl[i]].w = m_Coordinate[t][i+a*TexSize].x;
								dataTexC[i][countParticl[i]].w = m_Coordinate[t][i+a*TexSize].y;
								dataTexA[i][countParticl[i]].w = m_Coordinate[t][i+a*TexSize].z;

								countParticl[i]++;
							}
						}
					}
				}
				err += clFinish(GPUCommandQueue);
				if(err != CL_SUCCESS){pLog->SetError("Finish GPU Command Queue", 2);return;}
				ComputeSC(I, a*TexSize);
			}
			Sleep(1);
		}

		// ��� ���������� ������ ����� ����������������� ������
		err += clFinish(GPUCommandQueue);
		if(err != CL_SUCCESS){pLog->SetError("Finish GPU Command Queue", 2);return;}

////////////////////////////////////////////
		// �����������
		err = 0;
		cl_event  eventsCopy[4];
		err += clEnqueueWriteBuffer ( GPUCommandQueue, clmemFV, CL_TRUE, 0, sizeof(cl_float4)*countPart, m_Velocity[t], 0, NULL, &eventsCopy[0]);
		err += clEnqueueWriteBuffer ( GPUCommandQueue, clmemFC, CL_TRUE, 0, sizeof(cl_float4)*countPart, m_Coordinate[t], 0, NULL, &eventsCopy[1]);

		if(err!=CL_SUCCESS){pLog->SetError("Write-Arguments Error", 2); break;}

		float _T = m_paramColculate.T;
		err += clSetKernelArg(clKernel[1], 0, sizeof(cl_mem), (void*)&clmemFC);
		err += clSetKernelArg(clKernel[1], 1, sizeof(cl_mem), (void*)&clmemFV);
		err += clSetKernelArg(clKernel[1], 2, sizeof(cl_mem), (void*)&clmemE);
		err += clSetKernelArg(clKernel[1], 3, sizeof(cl_mem), (void*)&clmemB);
		err += clSetKernelArg(clKernel[1], 4, sizeof(cl_mem), (void*)&clmemSV);
		err += clSetKernelArg(clKernel[1], 5, sizeof(cl_mem), (void*)&clmemSC);
		err += clSetKernelArg(clKernel[1], 6, sizeof(cl_mem), (void*)&clmemSA);
		err += clSetKernelArg(clKernel[1], 7, sizeof(cl_float), (void*)&_T);

		if(err!=CL_SUCCESS){pLog->SetError("Setting-Arguments Error", 2);break;}

		//***********************EXECUTING KERNEL**********************
		WorkSize[0] = countPart;
		err += clEnqueueNDRangeKernel(GPUCommandQueue, clKernel[1], 1, NULL, WorkSize, NULL, 2, eventsCopy, NULL);
		err += clFlush(GPUCommandQueue);
		err += clFinish(GPUCommandQueue);
		if(err!=CL_SUCCESS){pLog->SetError("Executing Error", 2);break;}
		//***********************READING RESULTS**********************
		err += clEnqueueReadBuffer(GPUCommandQueue, clmemSV, CL_TRUE, 0, sizeof(cl_float4)*countPart, m_Velocity[cs], 0, NULL, NULL);
		err += clEnqueueReadBuffer(GPUCommandQueue, clmemSC, CL_TRUE, 0, sizeof(cl_float4)*countPart, m_Coordinate[cs], 0, NULL, NULL);
		err += clEnqueueReadBuffer(GPUCommandQueue, clmemSA, CL_TRUE, 0, sizeof(cl_float4)*countPart, m_Acceleration[cs], 0, NULL, NULL);

		if(err != CL_SUCCESS){pLog->SetError("Reading Results Error", 2);break;}

		m_curentCountSteps = cs;

		m_curentCompTime = GetTickCount() - m_startCompTime;
		if(m_curentCountSteps%100 == 0)
			m_computeTime.push_back(pair<int,DWORD>(m_curentCountSteps, GetTickCount() - m_startCompTime + m_oldCompTime));
	}
	m_Shift.insert(m_Shift.end(), m_curentCountParticls);

	m_computeTime.push_back(pair<int,DWORD>(m_curentCountSteps, GetTickCount() - m_startCompTime + m_oldCompTime));

	m_Computed = true;
}