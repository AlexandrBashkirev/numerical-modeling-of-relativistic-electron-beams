//
// RK2 - Runge Kutta 2
// SC - Space Charge
// GPU - OpenCL
// VA - for Velocity Acceleration
// Lag - effect of lag 
//

#ifndef _RK2_SC_GPU_VA_Lag_
#define _RK2_SC_GPU_VA_Lag_

#include "stdafx.h"
#include "../ICompute/c_GPUInit.h"
#include "../ICompute/c_ICompute.h"

static const int TexSize = 2048;

extern "C++"
{
	class EXPORT c_RK2_SC_GPU_VA_Lag_Diod : public c_ICompute, public c_GPUInit
	{
	public:
		void Create(s_paramColculate&);
		void Create(s_fullParamColculate&);
		void StartCompute();
		void StopCompute();

		c_RK2_SC_GPU_VA_Lag_Diod();
		~c_RK2_SC_GPU_VA_Lag_Diod();

		VECTOR4* getLastStepCoords()
		{
			vector<unsigned int> indexes;
			for(int i = 0; i < curentCountParticls(); i++)
			{
				if( m_Activity[curentCountSteps()-1][i])
					indexes.insert(indexes.end(), i);
			}

			int _curentCountParticls = indexes.size();

			VECTOR4* _C = new VECTOR4[_curentCountParticls];

			for(int i = 0; i < _curentCountParticls; i++)
				_C[i] = m_Coordinate[curentCountSteps()-1][ indexes[i] ];

			return _C;
		}

	private:
		cl_mem clmemFV;
		cl_mem clmemFC;

		cl_mem clmemSV;
		cl_mem clmemSC;
		cl_mem clmemSA;

		cl_mem clmemE;
		cl_mem clmemB;

		cl_mem clmemCountParticl;

		cl_mem clmemTexA;
		cl_mem clmemTexV;
		cl_mem clmemTexC;

		VECTOR4 dataTexA[TexSize][TexSize];
		VECTOR4 dataTexV[TexSize][TexSize];
		VECTOR4 dataTexC[TexSize][TexSize];

		int countParticl[TexSize];

		void checkCountPart(int cs);
		void ComputeSC( int countPartForLagField_i, int offset );
		void InitOCLBUF();
	};
}

#endif